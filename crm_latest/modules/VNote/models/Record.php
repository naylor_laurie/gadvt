<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

/**
 * ModComments Record Model
 */
class VNote_Record_Model extends Vtiger_Record_Model {
	
	public function getNoteTime() {
		$noteTime = $this->get('createdtime');
		return $noteTime;
	}
	
   public function getModifiedTime() {
		$noteTime = $this->get('modifiedtime');
		return $noteTime;
	}

	public function getId() {
        $id = $this->get('vnoteid');
		if(empty($id)) {
			return $this->get('id');
		}
     	return $this->get('vnoteid');
	  }
	
     public function getNoteByModel() {
		$noteBy = $this->get('smownerid');
		if($noteBy) {
		    $noteByModel = Vtiger_Record_Model::getInstanceById($noteBy, 'Users');
		    if(empty($noteByModel->entity->column_fields['user_name'])) {
			$activeAdmin = Users::getActiveAdminUser();
			$noteByModel = Vtiger_Record_Model::getInstanceById($activeAdmin->id, 'Users');
		    }
		    return $noteByModel;
		}
	}
  
    public function getImagePath() {
		$commentor = $this->getNoteByModel();
		if($commentor) {
			$customer = $this->get('customer');
            $imagePath = $commentor->getImageDetails();
			if (!empty($imagePath[0]['name'])) {
				return $imagePath[0]['path'] . '_' . $imagePath[0]['name'];
		    }
		}
		return false;
	}
	
	
	public static function getAllParentNotes($parentId) {
		$db = PearDatabase::getInstance();

		$listView = Vtiger_ListView_Model::getInstance('VNote');
		$queryGenerator = $listView->get('query_generator');
		$queryGenerator->setFields(array('createdtime', 'modifiedtime', 'related_to', 'id',
											'assigned_user_id', 'vnotetitle','userid'));
		$query = $queryGenerator->getQuery();

		//Condition are directly added as query_generator transforms the
		//reference field and searches their entity names
		$query = $query ." AND related_to = ? ORDER BY vtiger_crmentity.createdtime DESC";
		
		$result = $db->pquery($query, array($parentId));
		$rows = $db->num_rows($result);

		for ($i=0; $i<$rows; $i++) {
			$row = $db->query_result_rowdata($result, $i);
			$recordInstance = new self();
			$recordInstance->setData($row);
			$recordInstances[] = $recordInstance;
		}
		return $recordInstances;
	}
	
	public static function getInstanceById($record) {
		$db = PearDatabase::getInstance();
		$result = $db->pquery('SELECT vtiger_vnote.*, vtiger_crmentity.smownerid,
					vtiger_crmentity.createdtime, vtiger_crmentity.modifiedtime FROM vtiger_vnote
					INNER JOIN vtiger_crmentity ON vtiger_vnote.vnoteid = vtiger_crmentity.crmid
					WHERE vnoteid = ? AND deleted = 0', array($record));
		if($db->num_rows($result)) {
			$row = $db->query_result_rowdata($result, $i);
			$self = new self();
			$self->setData($row);
			return $self;
		}
		return false;
	}
	
	
}