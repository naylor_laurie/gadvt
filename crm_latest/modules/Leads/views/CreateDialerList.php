<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

class Leads_CreateDialerList_View extends Vtiger_Index_View {

	function process (Vtiger_Request $request) {
		
		$moduleName = $request->get("module");
		
		$viewer = $this->getViewer ($request);
		
		$this->initializeListViewContents($request, $viewer);

		$viewer->assign('MODULE_NAME',$moduleName);
		
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		
		$recordStructureInstance = Vtiger_RecordStructure_Model::getInstanceForModule($moduleModel);
			
		$viewer->assign('SEARCHABLE_MODULES', Vtiger_Module_Model::getSearchableModules());
		$advanceFilterOpsByFieldType = Vtiger_Field_Model::getAdvancedFilterOpsByFieldType();

		$viewer->assign('ADVANCED_FILTER_OPTIONS', Vtiger_Field_Model::getAdvancedFilterOptions());
		$viewer->assign('ADVANCED_FILTER_OPTIONS_BY_TYPE', $advanceFilterOpsByFieldType);
			
		$dateFilters = Vtiger_Field_Model::getDateFilterTypes();
		
		foreach($dateFilters as $comparatorKey => $comparatorInfo) {
			$comparatorInfo['startdate'] = DateTimeField::convertToUserFormat($comparatorInfo['startdate']);
			$comparatorInfo['enddate'] = DateTimeField::convertToUserFormat($comparatorInfo['enddate']);
			$comparatorInfo['label'] = vtranslate($comparatorInfo['label'],$module);
			$dateFilters[$comparatorKey] = $comparatorInfo;
		}
		
		$viewer->assign('DATE_FILTERS', $dateFilters);
		$viewer->assign('RECORD_STRUCTURE', $recordStructureInstance->getStructure());
		$viewer->assign('SOURCE_MODULE',$moduleName);
		$viewer->assign('SOURCE_MODULE_MODEL', $moduleModel);

		$viewer->view('CreateDialerList.tpl', $moduleName);
		
	}

	/*
	 * Function to initialize the required data in smarty to display the List View Contents
	 */
	public function initializeListViewContents(Vtiger_Request $request, Vtiger_Viewer $viewer) {

		$moduleName = $request->get("module");
		
		$pageNumber = $request->get('page');
		
		$orderBy = $request->get('orderby');
		
		$sortOrder = $request->get('sortorder');
		
		$currencyId = $request->get('currency_id');

		$cvId = '0';
		
		if(empty ($pageNumber)) {
			$pageNumber = '1';
		}

		$pagingModel = new Vtiger_Paging_Model();
		
		$pagingModel->set('page', $pageNumber);

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		
		$listViewModel = Vtiger_ListView_Model::getInstanceForPopup($moduleName);
		
		if(!empty($orderBy)) {
			$listViewModel->set('orderby', $orderBy);
			$listViewModel->set('sortorder', $sortOrder);
		}

		$this->listViewHeaders = $listViewModel->getListViewHeaders();
		
		$this->listViewEntries = $this->getListViewEntries($pagingModel,$listViewModel,$request);

		$noOfEntries = count($this->listViewEntries);

		if(empty($sortOrder)) {
			$sortOrder = "ASC";
		}
		
		if($sortOrder == "ASC") {
			$nextSortOrder = "DESC";
			$sortImage = "downArrowSmall.png";
		}else {
			$nextSortOrder = "ASC";
			$sortImage = "upArrowSmall.png";
		}

		$viewer->assign('ORDER_BY',$orderBy);
		$viewer->assign('SORT_ORDER',$sortOrder);
		$viewer->assign('NEXT_SORT_ORDER',$nextSortOrder);
		$viewer->assign('SORT_IMAGE',$sortImage);

		$viewer->assign('PAGING_MODEL', $pagingModel);
		$viewer->assign('PAGE_NUMBER',$pageNumber);

		$viewer->assign('LISTVIEW_ENTIRES_COUNT',$noOfEntries);
		$viewer->assign('LISTVIEW_HEADERS', $this->listViewHeaders);
		$viewer->assign('LISTVIEW_ENTRIES', $this->listViewEntries);
		
		if (PerformancePrefs::getBoolean('LISTVIEW_COMPUTE_PAGE_COUNT', false)) {
			
			if(!$this->listViewCount){
				$this->listViewCount = $this->getPageCount($listViewModel);
			}
			
			$totalCount = $this->listViewCount;
			$pageLimit = $pagingModel->getPageLimit();
			$pageCount = ceil((int) $totalCount / (int) $pageLimit);

			if($pageCount == 0){
				$pageCount = 1;
			}
			
			$viewer->assign('PAGE_COUNT', $pageCount);
			$viewer->assign('LISTVIEW_COUNT', $totalCount);
		}

		$viewer->assign('CURRENT_USER_MODEL', Users_Record_Model::getCurrentUserModel());
	}

	 /**
	 * Function to get the list of Script models to be included
	 * @param Vtiger_Request $request
	 * @return <Array> - List of Vtiger_JsScript_Model instances
	 */
	function getHeaderScripts(Vtiger_Request $request) {
		
		$headerScriptInstances = parent::getHeaderScripts($request);
		
		$moduleName = $request->getModule();
		$modulePopUpFile = 'modules.Vtiger.resources.Popup';
		unset($headerScriptInstances[$modulePopUpFile]);
		
		$jsFileNames = array();

		$jsFileNames[] = 'modules.Leads.resources.CreateDialerList';
		$jsFileNames[] = 'modules.Vtiger.resources.BaseList';
		
		$jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
		
		$headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
		
		return $headerScriptInstances;
	
	}
	
	public function getListViewEntries($pagingModel,$listViewModel,$request) {
		
		$db = PearDatabase::getInstance();

		$moduleName = $request->get("module");
		
		$moduleFocus = CRMEntity::getInstance($moduleName);
		
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		
		$user = Users_Record_Model::getCurrentUserModel();
		$queryGenerator = $listViewModel->get('query_generator');

		$listViewContoller = $listViewModel->get('listview_controller');
		
		$advFilterList = $request->get('advfilterlist');
		
		//used to show the save modify filter option
		
		$matchingRecords = array();
		
		if(is_array($advFilterList) && count($advFilterList) > 0) {
			
			$isAdvanceSearch = true;
			
            vimport('~~/modules/CustomView/CustomView.php');
            $customView = new CustomView($moduleName);
            $dateSpecificConditions = $customView->getStdFilterConditions();

			foreach ($advFilterList as $groupindex=>$groupcolumns) {
				$filtercolumns = $groupcolumns['columns'];
				if(count($filtercolumns) > 0) {
					$queryGenerator->startGroup('');
					foreach ($filtercolumns as $index=>$filter) {
						$nameComponents = explode(':',$filter['columnname']);
						if(empty($nameComponents[2]) && $nameComponents[1] == 'crmid' && $nameComponents[0] == 'vtiger_crmentity') {
							$name = $queryGenerator->getSQLColumn('id');
						} else {
							$name = $nameComponents[2];
						}
                        if(($nameComponents[4] == 'D' || $nameComponents[4] == 'DT') && in_array($filter['comparator'], $dateSpecificConditions)) {
                            $filter['stdfilter'] = $filter['comparator'];
                            $valueComponents = explode(',',$filter['value']);
                            if($filter['comparator'] == 'custom') {
                                $filter['startdate'] = DateTimeField::convertToDBFormat($valueComponents[0]);
                                $filter['enddate'] = DateTimeField::convertToDBFormat($valueComponents[1]);
                            }
                            $dateFilterResolvedList = $customView->resolveDateFilterValue($filter);
                            $value[] = $queryGenerator->fixDateTimeValue($name, $dateFilterResolvedList['startdate']);
                            $value[] = $queryGenerator->fixDateTimeValue($name, $dateFilterResolvedList['enddate'], false);
                            $queryGenerator->addCondition($name, $value, 'BETWEEN');
                        }else{
                            $queryGenerator->addCondition($name, $filter['value'], $filter['comparator']);
                        }
						$columncondition = $filter['column_condition'];
						if(!empty($columncondition)) {
							$queryGenerator->addConditionGlue($columncondition);
						}
					}
					$queryGenerator->endGroup();
					$groupConditionGlue = $groupcolumns['condition'];
					if(!empty($groupConditionGlue))
						$queryGenerator->addConditionGlue($groupConditionGlue);
				}
			}
		}	
			
		$query = $queryGenerator->getQuery();

		$startIndex = $pagingModel->getStartIndex();
		
		$pageLimit = $pagingModel->getPageLimit();
		
		$query .= " LIMIT $startIndex,".($pageLimit+1);
		
		$listResult = $db->pquery($query, array());
		
		$listViewRecordModels = array();
		
		$listViewEntries =  $listViewContoller->getListViewRecords($moduleFocus,$moduleName, $listResult);
		
		$pagingModel->calculatePageRange($listViewEntries);

		if($db->num_rows($listResult) > $pageLimit){
			array_pop($listViewEntries);
			$pagingModel->set('nextPageExists', true);
		}else{
			$pagingModel->set('nextPageExists', false);
		}
		
		$index = 0;
		
		foreach($listViewEntries as $recordId => $record) {
			$rawData = $db->query_result_rowdata($listResult, $index++);
			$record['id'] = $recordId;
			$listViewRecordModels[$recordId] = $moduleModel->getRecordFromArray($record, $rawData);
		}
		
		return $listViewRecordModels;
		
	}
	
	function getListViewCount(Vtiger_Request $request){
		
		$db = PearDatabase::getInstance();
		
		$moduleName = $request->get("module");
		
		$listViewModel = Vtiger_ListView_Model::getInstanceForPopup($moduleName);
		
		$queryGenerator = $listViewModel->get('query_generator');
		
		$advFilterList = $request->get('advfilterlist');
		
		if(is_array($advFilterList) && count($advFilterList) > 0) {
			
			$isAdvanceSearch = true;
			
            vimport('~~/modules/CustomView/CustomView.php');
            $customView = new CustomView($moduleName);
            $dateSpecificConditions = $customView->getStdFilterConditions();

			foreach ($advFilterList as $groupindex=>$groupcolumns) {
				$filtercolumns = $groupcolumns['columns'];
				if(count($filtercolumns) > 0) {
					$queryGenerator->startGroup('');
					foreach ($filtercolumns as $index=>$filter) {
						$nameComponents = explode(':',$filter['columnname']);
						if(empty($nameComponents[2]) && $nameComponents[1] == 'crmid' && $nameComponents[0] == 'vtiger_crmentity') {
							$name = $queryGenerator->getSQLColumn('id');
						} else {
							$name = $nameComponents[2];
						}
                        if(($nameComponents[4] == 'D' || $nameComponents[4] == 'DT') && in_array($filter['comparator'], $dateSpecificConditions)) {
                            $filter['stdfilter'] = $filter['comparator'];
                            $valueComponents = explode(',',$filter['value']);
                            if($filter['comparator'] == 'custom') {
                                $filter['startdate'] = DateTimeField::convertToDBFormat($valueComponents[0]);
                                $filter['enddate'] = DateTimeField::convertToDBFormat($valueComponents[1]);
                            }
                            $dateFilterResolvedList = $customView->resolveDateFilterValue($filter);
                            $value[] = $queryGenerator->fixDateTimeValue($name, $dateFilterResolvedList['startdate']);
                            $value[] = $queryGenerator->fixDateTimeValue($name, $dateFilterResolvedList['enddate'], false);
                            $queryGenerator->addCondition($name, $value, 'BETWEEN');
                        }else{
                            $queryGenerator->addCondition($name, $filter['value'], $filter['comparator']);
                        }
						$columncondition = $filter['column_condition'];
						if(!empty($columncondition)) {
							$queryGenerator->addConditionGlue($columncondition);
						}
					}
					$queryGenerator->endGroup();
					$groupConditionGlue = $groupcolumns['condition'];
					if(!empty($groupConditionGlue))
						$queryGenerator->addConditionGlue($groupConditionGlue);
				}
			}
		}
		
		$listQuery = $queryGenerator->getQuery();
		
        $position = stripos($listQuery, ' from ');
		
		if ($position) {
			$split = spliti(' from ', $listQuery);
			$splitCount = count($split);
			$listQuery = 'SELECT count(*) AS count ';
			for ($i=1; $i<$splitCount; $i++) {
				$listQuery = $listQuery. ' FROM ' .$split[$i];
			}
		}
		
		$listResult = $db->pquery($listQuery, array());
		
		return $db->query_result($listResult, 0, 'count');
	
	}
	
	/**
	 * Function to get the page count for list
	 * @return total number of pages
	 */
	function getPageCount(Vtiger_Request $request){
		
		$listViewCount = $this->getListViewCount($request);
		
		$pagingModel = new Vtiger_Paging_Model();
		
		$pageLimit = $pagingModel->getPageLimit();
		
		$pageCount = ceil((int) $listViewCount / (int) $pageLimit);

		if($pageCount == 0){
			$pageCount = 1;
		}
		$result = array();
		$result['page'] = $pageCount;
		$result['numberOfRecords'] = $listViewCount;
		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
	}
}