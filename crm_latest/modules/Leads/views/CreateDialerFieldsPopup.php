<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

class Leads_CreateDialerFieldsPopup_View extends Vtiger_Footer_View {
    
    function checkPermission(Vtiger_Request $request) {
		return true;
	}
	
	public function process(Vtiger_Request $request) {
		
		$moduleName = $request->getModule();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);		
		
		$fields = $this->getLeadsFields();
		
		$advfilterlist = '';
		if($request->get('advfilterlist') != ''){
			$advfilterlist = $request->get('advfilterlist');
		}
		$viewer = $this->getViewer($request);
		$viewer->assign('MODULE', $moduleName);
		
		$viewer->assign('VIEW', $request->get('view'));
		$viewer->assign('MODULE_MODEL', $moduleModel);
		$viewer->assign('FIELDS', $fields);
		$viewer->assign('ADV_FILTER_FIELDS', $advfilterlist);
		$viewer->assign('CURRENT_USER_MODEL', Users_Record_Model::getCurrentUserModel());
		
		$viewer->view('CreateDialerFieldsPopup.tpl', $moduleName);
		
	}
	
	function getLeadsFields()
	{
		global $adb;
		global $log;
		global $current_user;

		$module = "Leads";
		
		$tabid = getTabid($module);
		
		$params = array($tabid);

		require('user_privileges/user_privileges_'.$current_user->id.'.php');
		//Security Check
		if($is_admin == true || $profileGlobalPermission[1] == 0 || $profileGlobalPermission[2] ==0)
		{
			$sql = "select * from vtiger_field 
			where vtiger_field.tabid in (". generateQuestionMarks($tabid) .") 
			and vtiger_field.displaytype in (1,2,3) 
			and vtiger_field.presence in (0,2)";

			$sql.=" order by sequence";
		}
		else
		{

			$profileList = getCurrentUserProfileList();
			$sql = "select * from vtiger_field 
			inner join vtiger_profile2field on vtiger_profile2field.fieldid=vtiger_field.fieldid 
			inner join vtiger_def_org_field on vtiger_def_org_field.fieldid=vtiger_field.fieldid 
			where vtiger_field.tabid in (". generateQuestionMarks($tabid) .") 
			and vtiger_field.displaytype in (1,2,3) 
			and vtiger_profile2field.visible=0 
			and vtiger_def_org_field.visible=0 and vtiger_field.presence in (0,2)";
			if (count($profileList) > 0) {
				$sql .= " and vtiger_profile2field.profileid in (". generateQuestionMarks($profileList) .")";
				array_push($params, $profileList);
			}
			
			$sql.=" group by vtiger_field.fieldid order by sequence";
		}
		
		$result = $adb->pquery($sql, $params);
		$noofrows = $adb->num_rows($result);
		for($i=0; $i<$noofrows; $i++)
		{
			$fieldtablename = $adb->query_result($result,$i,"tablename");
			$fieldcolname = $adb->query_result($result,$i,"columnname");
			$fieldname = $adb->query_result($result,$i,"fieldname");
			$fieldtype = $adb->query_result($result,$i,"typeofdata");
			$uitype = $adb->query_result($result,$i,"uitype");
			$fieldtype = explode("~",$fieldtype);
			$fieldtypeofdata = $fieldtype[0];
			//$blockid = $adb->query_result($result, $i, "block");

			$fieldlabel = $adb->query_result($result,$i,"fieldlabel");
			
			$fieldlabel1 = str_replace(" ","_",$fieldlabel);
			$optionvalue = $fieldlabel.":".$fieldtablename.":".$fieldcolname.":".$fieldname;
			$optionvalue = $fieldtablename . ":" . $fieldcolname . ":" . $fieldname . ":" . $module . "_" .
					$fieldlabel1 . ":" . $fieldtypeofdata;
					
			$module_columnlist[$optionvalue] = $fieldlabel;
			
		}

		return $module_columnlist;
	}
	
}
