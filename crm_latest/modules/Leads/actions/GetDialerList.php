<?php

class Leads_GetDialerList_Action extends Vtiger_Action_Controller {

	public function checkPermission(Vtiger_Request $request) {
		return true;
	}

	public function process(Vtiger_Request $request) {
		
		$module = $request->getModule();
		
		$columnList = $request->get('columnsList');
		
		$listname = $request->get('listname');
		
		$advfilter = $request->get('advanced_filter');
		
		$headers = array();
		
		$rows_data = array();
		
		foreach($columnList as $key => $column_data){
			$column_parts = explode(":",$column_data);
			$column_name = $column_parts[1];
			$label1 = explode('_',$column_parts[3]);
			unset($label1[0]);
			$headers[$column_name] = implode(' ',$label1);				
		}

		$rows_data = $this->getRowData($columnList, $advfilter, $module);
		
		$document_id = '';
		
		if(!empty($rows_data)){
			$document_id = $this->save_document($listname, $module, $headers, $rows_data);
		}
		
		$result = array('success'=>true, 'message'=> $document_id);

		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
		
	}

	public function getRowData($columns, $advfilter, $module){
		global $adb;
		$listQuery = $this->getQuery($columns, $advfilter, $module, $adb);
		
		$queryResult = $adb->pquery($listQuery);

		$response = array();
		
		if(empty($columns)){
			$customView = new CustomView();
			$viewid = $customView->getViewId($module);	
			$columns = CustomView::getColumnsListByCvid($viewid);
		}
		$i = 0;
		
		while($record = $adb->fetchByAssoc($queryResult)){
			
			foreach($columns as $key => $column_data){
				$column_parts = explode(":",$column_data);
				$column_name = $column_parts[1];

				$response[$i][$column_name] = $record[$column_name];
			}			
			$i++;
		}
		
		return $response;
		
	}
	
	public function getQuery($columns, $advfilter, $module, $adb){
		
		$currentUser = vglobal('current_user');
		
		$focus = CRMEntity::getInstance($module);
			
		$queryGenerator = new QueryGenerator($module, $currentUser);
			
		$column_array = array();
		
		foreach($columns as $key => $column_data){
			$column_parts = explode(":",$column_data);
			$column_array[] = $column_parts[2];
		}
		
		if(!empty($column_array)){
			$queryGenerator->setFields($column_array);			
		}else{
			$customView = new CustomView();
			$viewid = $customView->getViewId($module);				
			$queryGenerator->initForCustomViewById($viewid);
		}
		
		$queryGenerator->parseAdvFilterList($advfilter);
		
		$listquery =  $queryGenerator->getQuery();
		
		return $listquery;
	}
	
	public function create_csv($headers, $rows_data, $module, $upload_file_path, $attachid){
		
		$header_content = '';
		$file_content = '';
		
		foreach($headers as $fieldlabel){
			$header_content .= $fieldlabel.',';
		}	
		$header_content .= "\r\n";
		
		foreach($rows_data as $key => $row){
			
			foreach($headers as $column => $fieldlabel){
				$value =  $row[$column];
				
				if($value == ''){
					$value1 = '--';
				}else if( in_array($column, array('smownerid','modifiedby','smcreatorid')) && $value != '' ){
					$value1 = getEntityName("Users", $value, false);
					if($value1[$value] == '')
						$value1 = getEntityName("Groups", $value, false);
					if($value1[$value] == '')
						$value1 = '--';
					else
						$value1 = $value1[$value];					
				}else{
					$value1 = $value;
				}
				$file_content .= $value1.',';
			}
			$file_content .= "\r\n";		
		}
		$file_content = $header_content.$file_content;
		$file_content = html_entity_decode($file_content, ENT_QUOTES, 'UTF-8');
		
		global $root_directory;
		
		$original_file_name = "Leads_List_".date("Ymd_Hi").".csv";
		
		$filename = $attachid . "_" . "Leads_List_".date("Ymd_Hi").".csv";    
		
		$uploadfile = $root_directory.$upload_file_path.$filename;
		
		file_put_contents($uploadfile,$file_content);
		
		return array("file_name" => $original_file_name, "file_path" => $root_directory.$upload_file_path);
	
	}
        
	public function save_document($listName, $module, $headers, $rows_data){
	
		global $adb, $current_user;
		
		$folderId = 1;
		
		$folderModel = Documents_Folder_Model::getInstance();
		$folderModel->set('foldername', $listName);
		$folderModel->set('description', $listName);
		
		if (!$folderModel->checkDuplicate()) {
			$folderModel->save();
			$folder_info = getInfoArray();
			$folderId = $folder_info['folderid'];
		}else{
			$result = $adb->pquery("SELECT folderid FROM vtiger_attachmentsfolder WHERE foldername = ?", array($listName));
			$num_rows = $adb->num_rows($result);
			if ($num_rows > 0) {
				$folderId = $adb->query_result($result, 0, 'folderid');
			}
		}
		
		$attachid = $adb->getUniqueId('vtiger_crmentity');
		
		$userid = $current_user->id;
		
		$upload_file_path = decideFilePath();	    
		
		$file_info = $this->create_csv($headers, $rows_data, $module, $upload_file_path, $attachid);
				
		$file_name = $file_info['file_name']; 
		
		$description = $file_name;
		
		$setype = "Leads Attachment";
		
		$mimetype = "application/vnd.ms-excel";
		
		//--- save entry of attachment
		
		$adb->pquery("INSERT INTO vtiger_crmentity(crmid, smcreatorid, smownerid,
				modifiedby, setype, description, createdtime, modifiedtime, presence, deleted)
				VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				Array($attachid, $userid, $userid, $userid, $setype, $description, $usetime, $usetime, 1, 0));
		$adb->pquery("INSERT INTO vtiger_attachments SET attachmentsid=?, name=?, description=?, type=?, path=?",
			Array($attachid, $file_name, $description, $mimetype, $file_info['file_path']));
		

		// Create document record
		require_once("modules/Documents/Documents.php");
		require_once("modules/Users/Users.php");
		$document = new Documents();
		$document->column_fields['notes_title']		 = $file_name;
		$document->column_fields['filename']		 = $file_name;
		$document->column_fields['filesize']		 = filesize($file_info['file_path']);
		$document->column_fields['filestatus']		 = 1;
		$document->column_fields['filelocationtype'] = 'I';
		$document->column_fields['folderid']         = $folderId;
		$document->column_fields['assigned_user_id'] = $userid;
		$document->save('Documents');

		// Link file attached to document
		$adb->pquery("INSERT INTO vtiger_seattachmentsrel(crmid, attachmentsid) VALUES(?,?)",
			Array($document->id, $attachid));
		
		return $document->id;
		
	}

		
		
		
}
