<?php

class Leads_ImportLeads_Action extends Vtiger_Action_Controller {

	public function checkPermission(Vtiger_Request $request) {
		return true;
	}

	public function process(Vtiger_Request $request) {
		
		$module = $request->getModule();
		
		//$columnList = $request->get('columnsList');
		
		$columnList = array(
			'vtiger_leaddetails:lead_no:lead_no',
			'vtiger_leaddetails:firstname:firstname',
			'vtiger_leaddetails:lastname:lastname',
			'vtiger_leaddetails:mobile:mobile',
		);
		
		$listname = $request->get('listname');
		
		$advfilter = $request->get('advanced_filter');
		
		$campaign = $request->get("campaign");
		
		$headers = array();		
		$rows_data = array();
		
		$rows_data = $this->getRowData($columnList, $advfilter, $module);
		
		foreach($rows_data as $lead){
			$phone = $lead['mobile'];
			
			$first_name = $lead['firstname'];
			
			$last_name = $lead['lastname'];
			
			$ch = curl_init(); // start CURL
			curl_setopt($ch, CURLOPT_URL, "http://209.133.197.133/vicidial/non_agent_api.php?source=test&user=admin&pass=gdgg5577dd&function=add_lead&
			phone_number=$phone&list_id=$listname&campaign_id=$campaign&first_name=$first_name&last_name=$last_name"); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			$response = curl_exec($ch); // connect and get your response
			curl_close($ch);
			
		}

		$result = array('success'=>true, 'message'=> $rows_data);

		$response = new Vtiger_Response();
		$response->setResult($result);
		$response->emit();
		
	}

	public function getRowData($columns, $advfilter, $module){
		global $adb;
		$listQuery = $this->getQuery($columns, $advfilter, $module, $adb);
		
		$queryResult = $adb->pquery($listQuery);

		$response = array();
		
		if(empty($columns)){
			$customView = new CustomView();
			$viewid = $customView->getViewId($module);	
			$columns = CustomView::getColumnsListByCvid($viewid);
		}
		$i = 0;
		
		while($record = $adb->fetchByAssoc($queryResult)){
			
			foreach($columns as $key => $column_data){
				$column_parts = explode(":",$column_data);
				$column_name = $column_parts[1];

				$response[$i][$column_name] = $record[$column_name];
			}			
			$i++;
		}
		
		return $response;
		
	}
	
	public function getQuery($columns, $advfilter, $module, $adb){
		
		$currentUser = vglobal('current_user');
		
		$focus = CRMEntity::getInstance($module);
			
		$queryGenerator = new QueryGenerator($module, $currentUser);
			
		$column_array = array();
		
		foreach($columns as $key => $column_data){
			$column_parts = explode(":",$column_data);
			$column_array[] = $column_parts[2];
		}
		
		if(!empty($column_array)){
			$queryGenerator->setFields($column_array);			
		}else{
			$customView = new CustomView();
			$viewid = $customView->getViewId($module);				
			$queryGenerator->initForCustomViewById($viewid);
		}
		
		$queryGenerator->parseAdvFilterList($advfilter);
		
		$listquery =  $queryGenerator->getQuery();
		
		return $listquery;
	}
}
