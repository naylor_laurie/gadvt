<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

class PBXManager_AddCallLog_View extends Vtiger_Index_View {

	public function process (Vtiger_Request $request) {
                $record = $request->get('record');
		$number = $request->get('callid');
// hangup call
        $adb = PearDatabase::getInstance();
        $query="select channel1,channel2 from vtiger_pbxmanager where customernumber='$number' order by pbxmanagerid desc limit 1"; //and customer='$record'
        $rcl= $adb->query($query) or die('error');
        $numrcl=$adb->num_rows($rcl);
        if($numrcl>0) {
                $tempArr=array();
                $channel1 = $adb->query_result($rcl, 0,'channel1');
                $channel2 = $adb->query_result($rcl, 0,'channel2');
		$gad_url=vglobal('gad_URL');
	 	$gad_url.="asterisk-php-api/run.php?c1='$channel1'&c2='$channel2'";
// sent api

                $ch = curl_init(); // start CURL
                curl_setopt($ch, CURLOPT_URL, $gad_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                $response = curl_exec($ch); // connect and get your response
                curl_close($ch);
	}
		
		$viewer = $this->getViewer($request);
		$moduleName = $request->get('module');
		
		$record = $request->get('record');
		
		$c_module = getSalesEntityType($record);
		global $adb;
		
		if(getSalesEntityType($record) == "Leads"){
			$LeadStatusModel = Vtiger_Field_Model::getInstance("leadstatus", Vtiger_Module_Model::getInstance("Leads"));
			$viewer->assign("LEADSTATUS", $LeadStatusModel->getPickListValues());
		} else {
			$LeadStatusModel = Vtiger_Field_Model::getInstance("leadstatus", Vtiger_Module_Model::getInstance("Leads"));
			$viewer->assign("LEADSTATUS", $LeadStatusModel->getPickListValues());
		}
		
	    $viewer->assign('RECORD', $record);
		$viewer->assign('MODULE',$moduleName);
		$viewer->view('AddCallLog.tpl', "Vtiger");
	}
}
