<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
require_once('modules/ModComments/ModComments.php');

class PBXManager_SaveCallLog_Action extends Vtiger_Action_Controller{
    
    public function checkPermission(Vtiger_Request $request) {
		return true;
	}
    
    public function process(Vtiger_Request $request) {
		global $current_user, $adb;
		
		$record = $request->get('record');
		
		$content = $request->get('commentcontent');
		$Comment_obj = CRMEntity::getInstance("ModComments");
		$Comment_obj->column_fields['assigned_user_id'] = $current_user->id;
		$Comment_obj->column_fields['commentcontent'] = $content;
		$Comment_obj->column_fields['related_to'] = $record;
		$Comment_obj->saveentity('ModComments');
		$callstatus= $request->get('lead_status');
		 
		
		$se_type = getSalesEntityType($record);
		
		//lead status update
		$obj = CRMEntity::getInstance($se_type);
		$obj->mode = 'edit';
		$obj->id = $record;
		$obj->retrieve_entity_info($record, $se_type);
		
		if($se_type == "Contacts"){
			//$obj->column_fields['cf_775'] = $request->get('status');
		} else {
			$obj->column_fields['leadstatus'] = $request->get('lead_status');
		}
		
		$obj->save($se_type);
		$sql = "update vtiger_pbxmanager set status = ?, comment = ? where customer = ? and user = ? order by pbxmanagerid desc limit 1";
		$adb->pquery($sql ,	array($request->get('lead_status'), $content, $record , $current_user->id)) or die("Syntax Error: $sql");;

	}
}
?>
