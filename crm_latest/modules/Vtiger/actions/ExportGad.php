<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_ExportGad_Action extends Vtiger_Index_View {

	function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		$currentUserPriviligesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		if(!$currentUserPriviligesModel->hasModuleActionPermission($moduleModel->getId(), 'Export')) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}
	}

	function process(Vtiger_Request $request) {
		$type = $request->get('type');
                if($type == "uselist"){
                    $listid=$request->get('all_list');
                }else{
                    $list_id=$request->get('$list_id');
                    $list_name=$request->get('list_name');
		    $campaign_id=$request->get('campaign');
		    $gad_URL = vglobal('gad_URL');
                    // create new list, source should be list name in vtiger
                    $url = $gad_URL."/non_agent_api.php?source=vtiger&user=admin&pass=gdgg5577dd&function=add_list&list_id=".$list_id."&list_name=".$list_name."&campaign_id=".$campaign_id;
                    $ch = curl_init(); // start CURL
                    curl_setopt($ch, CURLOPT_URL, urlencode($url)); 
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
                    curl_setopt($ch, CURLOPT_HTTPGET, true);
                    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                    $response = curl_exec($ch); // connect and get your response
                    curl_close($ch);
                    if(stristr($response,"error")){
                        exit("Following Error Occurred<br />".$response);
                    }
                }
                $viewer = $this->getViewer($request);
		$ch = curl_init(); // start CURL
		curl_setopt($ch, CURLOPT_URL, $gad_URL."non_agent_api.php?user=admin&pass=gdgg5577dd&function=list_fields&listid=12"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		$response = curl_exec($ch); // connect and get your response
		curl_close($ch);
		$list_fields = json_decode($response, true);
		
		$viewer->assign('LIST_FIELDS', $list_fields);
		

		$page = $request->get('page');

		$viewer->assign('MODULE','ExportGad');
// fetch crm fields
                $moduleName = $request->getModule();
               
                $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
                $moduleMeta = $moduleModel->getModuleMeta();
                $viewer->assign('AVAILABLE_FIELDS', $moduleMeta->getImportableFields($moduleName));
                $viewer->assign('ENCODED_MANDATORY_FIELDS', Zend_Json::encode($moduleMeta->getMandatoryFields($moduleName)));
                $viewer->assign('SAVED_MAPS', Import_Map_Model::getAllByModule($moduleName));
                
                $viewer->view('ExportGadStep2.tpl', "ExportGad");
	}
}
