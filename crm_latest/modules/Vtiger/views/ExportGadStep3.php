<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_ExportGadStep3_View extends Vtiger_Index_View {

	function checkPermission(Vtiger_Request $request) {
		return true;
	}

	function process(Vtiger_Request $request) {
		$list_name=$request->get("list_name");
		$fields=$request->get("mapped_fields");
		$gad_URL=vglobal("gad_URL");
/* fetch fields and map */
                $ch = curl_init(); // start CURL
                curl_setopt($ch, CURLOPT_URL, $gad_URL."non_agent_api.php?user=admin&pass=gdgg5577dd&function=list_fields&listid=".$list_id);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                $response = curl_exec($ch); // connect and get your response
                curl_close($ch);
                $list_fields = array_flip(json_decode($response, true));

		$newArr=array_combine(array_keys($list_fields),array_values($fields));
		$fields_unique= array_unique($newArr);
		foreach($fields_unique as $field){
			$vTgierfields[]='vtiger_leaddetails:'.$field.":".$field;
		}
                $columnList = $vTgierfields;
		// FIlter
        $listname = $request->get('listname');
	$listcolumn= $request->get('list_column');
	$advfilter=array();
	$advfilter[1]['columns'][0]=array('columnname'=>'vtiger_leadscf:'.$listcolumn.':'.$listcolumn.':Leads_List_Name:V',
				'comparator'=>'e',
				'value'=>$list_name,
				'column_condition'=>'');
                $module = $request->getModule();
		$list_id= $request->get('list_id');
                $headers = array();
                $rows_data = array();

		$rows_data = $this->getRowData($columnList, $advfilter, $module);
		$failed=0;
		$added=0;
		foreach($rows_data as $lead){
                        $phone = $lead['mobile'];
                        $first_name = $lead['firstname'];
			$insert_arr=array_combine(array_keys($fields_unique),array_values($lead));
			if(sizeof($insert_arr)) {
				// create insert url
					 $urlArr[]="list_id=".$list_id;
				foreach($insert_arr as $ins_key=>$ins_value){
					$urlArr[]=$ins_key."=".urlencode($ins_value);
				}
				 $insertUrl="&".implode("&",$urlArr);
			}
                        $ch = curl_init(); // start CURL
                        curl_setopt($ch, CURLOPT_URL, $gad_URL."non_agent_api.php?source=test&user=admin&pass=gdgg5577dd&function=add_lead".$insertUrl);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
                        curl_setopt($ch, CURLOPT_HTTPGET, true);
                        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                         $response = curl_exec($ch); // connect and get your response
                        curl_close($ch);
			if(stristr($response,"success")) $added++;
			else $failed++;
		}
		
                $viewer = $this->getViewer($request);
                $viewer->assign('success',$added);
                $viewer->assign('failed',$failed);

                $viewer->view('ExportGadStep3.tpl', "ExportGad");

	}

        public function getRowData($columns, $advfilter, $module){
                global $adb;
                $listQuery = $this->getQuery($columns, $advfilter, $module, $adb);

                $queryResult = $adb->pquery($listQuery);

                $response = array();

                if(empty($columns)){
                        $customView = new CustomView();
                        $viewid = $customView->getViewId($module);
                        $columns = CustomView::getColumnsListByCvid($viewid);
                }
                $i = 0;

                while($record = $adb->fetchByAssoc($queryResult)){

                        foreach($columns as $key => $column_data){
                                $column_parts = explode(":",$column_data);
                                $column_name = $column_parts[1];

                                $response[$i][$column_name] = $record[$column_name];
                        }
                        $i++;
                }

                return $response;

        }
		
        public function getQuery($columns, $advfilter, $module, $adb){

                $currentUser = vglobal('current_user');

                $focus = CRMEntity::getInstance($module);

                $queryGenerator = new QueryGenerator($module, $currentUser);

                $column_array = array();

                foreach($columns as $key => $column_data){
	                        $column_parts = explode(":",$column_data);
		                        $column_array[] = $column_parts[2];
		                }

                if(!empty($column_array)){
	                        $queryGenerator->setFields($column_array);
		                }else{
			                        $customView = new CustomView();
				                        $viewid = $customView->getViewId($module);
				                        $queryGenerator->initForCustomViewById($viewid);
					                }

                $queryGenerator->parseAdvFilterList($advfilter);

                $listquery =  $queryGenerator->getQuery();

                return $listquery;
        }

	}
