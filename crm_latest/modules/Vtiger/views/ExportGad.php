<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_ExportGad_View extends Vtiger_Index_View {

	function checkPermission(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);

		$currentUserPriviligesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		if(!$currentUserPriviligesModel->hasModuleActionPermission($moduleModel->getId(), 'Export')) {
			throw new AppException(vtranslate('LBL_PERMISSION_DENIED'));
		}
	}

	function process(Vtiger_Request $request) {
		$gad_URL= vglobal("gad_URL");
		$ch = curl_init(); // start CURL
		curl_setopt($ch, CURLOPT_URL, $gad_URL."non_agent_api.php?user=admin&pass=gdgg5577dd&function=lists_list"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		$response = curl_exec($ch); // connect and get your response
		curl_close($ch);
		
		$response = json_decode($response, true);
		
		$lists = array();
		
		foreach($response as $list){
			if($list['active'] == 'Y'){
				$lists[$list['list_id']] = $list['list_name'];
			}
		}
		

                $viewer = $this->getViewer($request);
		$viewer->assign('LIST_NAMES', $lists);
		$ch = curl_init(); // start CURL
		curl_setopt($ch, CURLOPT_URL, $gad_URL."non_agent_api.php?user=admin&pass=gdgg5577dd&function=campaigns_list"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		$response = curl_exec($ch); // connect and get your response
		curl_close($ch);
		$response = json_decode($response, true);
		
		$campaigns = array();
		
		foreach($response as $campaign){
			if($campaign['active'] == 'Y'){
				$campaigns[$campaign['campaign_id']] = $campaign['campaign_name'];
			}
		}
		
		$viewer->assign('CAMPAIGN_NAMES', $campaigns);
		
		$source_module = $request->getModule();
		$viewId = $request->get('viewname');
		$selectedIds = $request->get('selected_ids');
		$excludedIds = $request->get('excluded_ids');

		$page = $request->get('page');

		$viewer->assign('SELECTED_IDS', $selectedIds);
		$viewer->assign('EXCLUDED_IDS', $excludedIds);
		$viewer->assign('VIEWID', $viewId);
		$viewer->assign('PAGE', $page);
		$viewer->assign('SOURCE_MODULE', $source_module);
		$viewer->assign('MODULE','ExportGad');
        
        $searchKey = $request->get('search_key');
        $searchValue = $request->get('search_value');
		$operator = $request->get('operator');
        if(!empty($operator)) {
			$viewer->assign('OPERATOR',$operator);
			$viewer->assign('ALPHABET_VALUE',$searchValue);
            		$viewer->assign('SEARCH_KEY',$searchKey);
		}
	$viewer->assign('SEARCH_PARAMS', $request->get('search_params'));
	// fetch list name
	// get list name column
	$adb = PearDatabase::getInstance();
	$query=" select columnname from vtiger_field where fieldlabel='List Name'";
	$rcl= $adb->query($query) or die('error');
	$numrcl=$adb->num_rows($rcl);
	if($numrcl>0) {
		$tempArr=array();
		$rowrcl = $adb->query_result($rcl, 0,'columnname');
                $viewer->assign('LIST_COLUMN',$rowrcl);

		$query=" select $rowrcl from vtiger_leadscf order by leadid desc limit 1";
		$rcl= $adb->query($query) or die('error 2');
		$numrcl=$adb->num_rows($rcl);
		if($numrcl>0) {
			$tempArr=array();
			$list_name = $adb->query_result($rcl, 0,$rowrcl);
			$viewer->assign('LIST_NAME',$list_name);
		}
	}
	
		$viewer->view('ExportGad.tpl', $source_module);
	}
}
