<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Vtiger_ExportGadStep2_View extends Vtiger_Index_View {

	function checkPermission(Vtiger_Request $request) {
		return true;
	}

	function process(Vtiger_Request $request) {
		$type = $request->get('type');
		$gad_URL= vglobal("gad_URL");
                if($type == "uselist"){
                    $list_id=$request->get('all_list');
                }else{
                    $list_id=$request->get('list_id');
                    $new_list_name=$request->get('new_list_name');
                    $campaign_id=$request->get('campaign');
                    // create new list, source should be list name in vtiger
                    $url = $gad_URL."non_agent_api.php?source=vtiger&user=admin&pass=gdgg5577dd&function=add_list&active=Y&list_id=".$list_id."&campaign_id=".$campaign_id."&list_name=".urlencode($new_list_name);
                    $ch = curl_init(); // start CURL
                    curl_setopt($ch, CURLOPT_URL, $url); 
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
                    curl_setopt($ch, CURLOPT_HTTPGET, true);
                    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                    $response = curl_exec($ch); // connect and get your response
                    curl_close($ch);
                    if(stristr($response,"error")){
			    exit("<strong><font style='color:red'>Following Error Occurred<br />".$response."</font></strong><br> ".
				    '<button name="goback" onclick="window.history.back()" class="edit btn btn-danger"><strong>Go back</strong></button>');
                    }
		}
		$list_name=$request->get('list_name');
                $viewer = $this->getViewer($request);
		$ch = curl_init(); // start CURL
		curl_setopt($ch, CURLOPT_URL, $gad_URL."non_agent_api.php?user=admin&pass=gdgg5577dd&function=list_fields&listid=".$list_id); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // get the response as a variable
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		$response = curl_exec($ch); // connect and get your response
		curl_close($ch);
		$list_fields = json_decode($response, true);
		$list_column= $request->get('list_column');	
		$viewer->assign('LIST_FIELDS', $list_fields);
		$viewer->assign('LIST_ID',$list_id);
                $viewer->assign('LIST_COLUMN',$list_column);

		$page = $request->get('page');

		$viewer->assign('MODULE','ExportGad');
// fetch crm fields
                $moduleName = $request->getModule();
               
		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
                $moduleMeta = $moduleModel->getModuleMeta();
		$viewer->assign('AVAILABLE_FIELDS', $moduleMeta->getImportableFields($moduleName));
		//$mandatory=array("phone_number" 
                //$viewer->assign('ENCODED_MANDATORY_FIELDS', Zend_Json::encode($moduleMeta->getMandatoryFields($moduleName)));
		$viewer->assign('SAVED_MAPS', Import_Map_Model::getAllByModule($moduleName));
		$viewer->assign('LIST_NAME',$list_name);
                $viewer->view('ExportGadStep2.tpl', "ExportGad");
	}
}
