<?php
	
	include_once("includes/main/WebUI.php");
	
	$user_obj = CRMEntity::getInstance("Users");
	$user_obj->id = 1;
	$user_obj->retrieve_entity_info($userid, "Users");
	vglobal("current_user", $user_obj);
	
	$user_name = $_REQUEST['user_name'];
	
	global $adb;
	
	$user_result = $adb->pquery("select * from vtiger_users where user_name = ?", array($user_name));
	
	if($adb->num_rows($user_result)){
		$user_id = $adb->query_result($user_result, 0, "id");
		$user_obj = CRMEntity::getInstance("Users");
		$user_obj->id = $user_id;
		$user_obj->retrieve_entity_info($user_id, "Users");
		$user_obj->mode = "edit";
		$user_obj->save("Users");
	}
?>