<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

$languageStrings = Array(
	'CallHistory' => 'CallHistory',
	'SINGLE_CallHistory Summary' => 'CallHistory Summary',
	'SINGLE_CallHistory Details' => 'CallHistory Details',
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_CallHistory_INFORMATION' => 'CallHistory Information',
    'SINGLE_CallHistory'=>'CallHistory',
	
);

?>
