<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

$languageStrings = Array(
	'VNote' => 'Notes',
	'SINGLE_VNote Summary' => 'Note Summary',
	'SINGLE_VNote Details' => 'Note Details',
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_VNote_INFORMATION' => 'Note Information',
    'SINGLE_VNote'=>'Notes',
);

?>
