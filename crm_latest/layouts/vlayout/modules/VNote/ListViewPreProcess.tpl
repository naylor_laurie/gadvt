{strip}
{include file="Header.tpl"|vtemplate_path:$MODULE}
{include file="BasicHeader.tpl"|vtemplate_path:$MODULE}
<div class="bodyContents">
	<div class="mainContainer row-fluid">
	{assign var=LEFTPANELHIDE value=$CURRENT_USER_MODEL->get('leftpanelhide')}
		<div class="span2{if $LEFTPANELHIDE eq '1'} hide {/if} row-fluid" id="leftPanel" style="min-height:550px;">
			<div class="row-fluid">
			   <div class="sideBarContents">
					<div class="quickLinksDiv">
						{foreach item=SIDEBARLINK from=$QUICK_LINKS['SIDEBARLINK']}
					        {assign var=SIDE_LINK_URL value=decode_html($SIDEBARLINK->getUrl())}
							{assign var="EXPLODED_PARSE_URL" value=explode('?',$SIDE_LINK_URL)}
							{assign var="COUNT_OF_EXPLODED_URL" value=count($EXPLODED_PARSE_URL)}
							{if $COUNT_OF_EXPLODED_URL gt 1}
								{assign var="EXPLODED_URL" value=$EXPLODED_PARSE_URL[$COUNT_OF_EXPLODED_URL-1]}
							{/if}
							{assign var="PARSE_URL" value=explode('&',$EXPLODED_URL)}
							{assign var="CURRENT_LINK_VIEW" value='view='|cat:$CURRENT_VIEW}
							{assign var="LINK_LIST_VIEW" value=in_array($CURRENT_LINK_VIEW,$PARSE_URL)}
							{assign var="CURRENT_MODULE_NAME" value='module='|cat:$MODULE}
							{assign var="IS_LINK_MODULE_NAME" value=in_array($CURRENT_MODULE_NAME,$PARSE_URL)}
							<p onclick="window.location.href='{$SIDEBARLINK->getUrl()}'" id="{$MODULE}_sideBar_link_{Vtiger_Util_Helper::replaceSpaceWithUnderScores($SIDEBARLINK->getLabel())}"
							   class="{if $LINK_LIST_VIEW and $IS_LINK_MODULE_NAME}selectedQuickLink {else}unSelectedQuickLink{/if}"><a class="quickLinks" href="{$SIDEBARLINK->getUrl()}">
									<strong>Analytical Record</strong>
							</a></p>
						{/foreach}
					</div>
			  </div>
            </div>
		</div>
		<div class="contentsDiv {if $LEFTPANELHIDE neq '1'} span10 {/if}marginLeftZero" id="rightPanel" style="min-height:550px;">
			<div id="toggleButton" class="toggleButton" title="{vtranslate('LBL_LEFT_PANEL_SHOW_HIDE', 'Vtiger')}">
				<i id="tButtonImage" class="{if $LEFTPANELHIDE neq '1'}icon-chevron-left{else}icon-chevron-right{/if}"></i>
			</div>
{/strip}