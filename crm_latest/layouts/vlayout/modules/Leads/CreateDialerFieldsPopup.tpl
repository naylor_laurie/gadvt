{strip}
<div class = "modalwindow">
	
	<div class = "modal-header contentsBackground">
		<button type = "button" class = "close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Create List</h3>
	</div>
	
	<form class="form-horizontal dialerfieldsview" id="dialerfieldsview" method="post" action="index.php">
		<div class = "modal-body">
			
			<input type="hidden" name="module" value="{$MODULE}" >
			<input type="hidden" name="action" value="GetDialerList" >
			<input type="hidden" name="advanced_filter" id="advanced_filter" value="{Vtiger_Util_Helper::toSafeHTML(ZEND_JSON::encode($ADV_FILTER_FIELDS))}" >

			<div class="row-fluid">
				
				<div class = "control-group">
					<label class ="control-label" style = "width:104px;">
						<span class="redColor">*</span>
						<strong>Enter List Name</strong>
					</label>
					<div class = "controls" style = "margin-left:107px;">
						<input type="text" id="listname" name="listname" value=""  data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]"/>
					</div>
				</div>
					
				<div class="row-fluid padding1per">
					<strong>Select Columns To get data (Maximum 15) </strong>
				</div>
				 
				<div class="row-fluid padding1per" id = "dialer_popup_container">
					<select data-placeholder="Add Columns" id="columnsList" class="select2-container span11 columns" multiple name="columnsList[]">
						{foreach key=FIELD_DATA item=FIELD_NAME from=$FIELDS}
							<option value="{$FIELD_DATA}">{vtranslate($FIELD_NAME, $MODULE)}</option>
						{/foreach}
					</select>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" class="btn btn-success nextStep"><strong>Get CSV</strong></button>&nbsp;&nbsp;
		</div>
	</form>
</div>
{/strip}