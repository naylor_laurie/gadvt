
{strip}
<div class = "modalwindow">
	
	<div class = "modal-header contentsBackground">
		<button type = "button" class = "close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Select Fields</h3>
	</div>
	
	<form class="form-horizontal" id="importstep1" method="post" action="index.php" enctype="multipart/form-data">
		<div class = "modal-body">
			
			<input type="hidden" name="module" value="{$MODULE}" />
			<input type="hidden" name="view" value="Import" />
			<input type="hidden" name="mode" value="uploadAndParse" />
			<input type="hidden" name="is_scheduled" value="0" />
			<input type="hidden" name="file_encoding" value="UTF-8" />
			<input type="hidden" name="delimiter" value="," />
			<input type="hidden" name="has_header" value="on" />
			<input type="hidden" name="type" value="csv" />
			
			<input type="hidden" name="importUploadSize" value="{$IMPORT_UPLOAD_SIZE}" />
			<input type="hidden" name="importUploadSizeMb" value="" />
			
			<div class="row-fluid">
				
				<div class = "control-group">
					<label class ="control-label" style = "width:101px;">
						<strong>Enter List Name</strong>
					</label>
					<div class = "controls" style = "/*margin-left:107px;*/">
						<input type="text" id="listname" name="listname" value=""/>
					</div>
				</div>
					
				<div class = "control-group">
					<label class ="control-label">
						<strong>Select File To Import Data <br> (Only .csv files allowed) </strong>
					</label>
					<div class = "controls">
						<input type="file" name="import_file" id="import_file" />
					</div>
				</div>
				 
				<div class = "control-group">				
					<label class="control-label"> 
						<strong> Duplicate Handling </strong>
					</label>
					<div class="controls">
						<input type="checkbox" class="font-x-small" id="auto_merge" name="auto_merge"/>
					</div>
				</div>


			<table width="100%" cellspacing="0" cellpadding="5" id="duplicates_merge_configuration" style="display:none;">
				<tr>
					<td>
						<span class="font-x-small">Specify Merge Type</span>&nbsp;&nbsp;
						<select name="merge_type" id="merge_type" class="font-x-small">
							{foreach key=_MERGE_TYPE item=_MERGE_TYPE_LABEL from=$AUTO_MERGE_TYPES}
							<option value="{$_MERGE_TYPE}">{$_MERGE_TYPE_LABEL|@vtranslate:$MODULE}</option>
							{/foreach}
						</select>
					</td>
				</tr>
				<tr>
					<td class="font-x-small">Select Merge Fields</td>
				</tr>
				<tr>
					<td>
						<table class="calDayHour" cellpadding="5" cellspacing="0">
							<tr>
								<td><b>Available Fields</b></td>
								<td></td>
								<td><b>Selected Fields</b></td>
							</tr>
							<tr>
								<td>
									<select id="available_fields" multiple size="10" name="available_fields" class="txtBox" style="width: 100%">
										{foreach key=_FIELD_NAME item=_FIELD_INFO from=$AVAILABLE_FIELDS}
										<option value="{$_FIELD_NAME}">{$_FIELD_INFO->getFieldLabelKey()|@vtranslate:$FOR_MODULE}</option>
										{/foreach}
									</select>
								</td>
								<td width="6%">
									<div align="center">
										<input type="button" name="Button" value="&nbsp;&rsaquo;&rsaquo;&nbsp;" onClick="Leads_CreateDialerList_Js.copySelectedOptionsImport('#available_fields', '#selected_merge_fields')" class="crmButton font-x-small importButton" /><br /><br />
										<input type="button" name="Button1" value="&nbsp;&lsaquo;&lsaquo;&nbsp;" onClick="Leads_CreateDialerList_Js.removeSelectedOptionsImport('#selected_merge_fields')" class="crmButton font-x-small importButton" /><br /><br />
									</div>
								</td>
								<td>
									<input type="hidden" id="merge_fields" size="10" name="merge_fields" value="" />
									<select id="selected_merge_fields" size="10" name="selected_merge_fields" multiple class="txtBox" style="width: 100%">
										{foreach key=_FIELD_NAME item=_FIELD_INFO from=$ENTITY_FIELDS}
										<option value="{$_FIELD_NAME}">{$_FIELD_INFO->getFieldLabelKey()|@vtranslate:$FOR_MODULE}</option>
										{/foreach}
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>



			</div>
		</div>
		<div class="modal-footer">
			<button type="submit" class="import btn btn-success nextStep"><strong>Next</strong></button>&nbsp;&nbsp;
		</div>
	</form>
</div>
{/strip}