/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
Vtiger_AdvanceSearch_Js("Leads_SearchLeads_Js",{
    
},{

	popupPageContentsContainer : false,
	advanceFilterInstance: false,
	
	/**
	 * Function to get Order by
	 */
	getOrderBy : function(){
		return jQuery('#orderBy').val();
	},

	/**
	 * Function to get Sort Order
	 */
	getSortOrder : function(){
		return jQuery("#sortOrder").val();
	},

	/**
	 * Function to get Page Number
	 */
	getPageNumber : function(){
		return jQuery('#pageNumber').val();
	},

	getPopupPageContainer : function(){
		if(this.popupPageContentsContainer == false) {
			this.popupPageContentsContainer = jQuery('#popupPageContainer');
		}
		return this.popupPageContentsContainer;

	},


	getView : function(){
	    var view = jQuery('#view').val();
	    if(view == '') {
		    view = 'PopupAjax';
	    } else {
		    view = view+'Ajax';
	    }
	    return view;
	},


	getListViewEntries: function(e){
		var thisInstance = this;
		var row  = jQuery(e.currentTarget);
		var dataUrl = row.data('url');
		if(typeof dataUrl != 'undefined'){
			dataUrl = dataUrl+'&currency_id='+jQuery('#currencyId').val();
		    AppConnector.request(dataUrl).then(
			function(data){
				for(var id in data){
				    if(typeof data[id] == "object"){
					var recordData = data[id];
				    }
				}
				thisInstance.done(recordData, thisInstance.getEventName());
				e.preventDefault();
			},
			function(error,err){

			}
		    );
		} else {
		    var id = row.data('id');
		    var recordName = row.data('name');
			var recordInfo = row.data('info');
		    var response ={};
		    response[id] = {'name' : recordName,'info' : recordInfo} ;
			thisInstance.done(response, thisInstance.getEventName());
		    e.preventDefault();
		}

	},

	/**
	 * Function to get complete params
	 */
	getCompleteParams : function(){
		var params = {};
		
		params['view'] = this.getView();
		params['orderby'] =  this.getOrderBy();
		params['sortorder'] =  this.getSortOrder();
		params['page'] = this.getPageNumber();
		params['module'] = app.getModuleName();
		
		if(this.advanceFilterInstance){
		    params['advfilterlist'] = JSON.stringify(this.advanceFilterInstance.getValues());
		}
		
		return params;
	},

	/**
	 * Function to get Page Records
	 */
	getPageRecords : function(params){
		var thisInstance = this;
		var aDeferred = jQuery.Deferred();
		var progressIndicatorElement = jQuery.progressIndicator({
			'position' : 'html',
			'blockInfo' : {
				'enabled' : true
			}
		});
		Vtiger_BaseList_Js.getPageRecords(params).then(
				function(data){
					jQuery('#popupContents').html(data);
					progressIndicatorElement.progressIndicator({
						'mode' : 'hide'
					})
					thisInstance.calculatePages().then(function(data){
						aDeferred.resolve(data);
					});
				},

				function(textStatus, errorThrown){
					aDeferred.reject(textStatus, errorThrown);
				}
			);
		return aDeferred.promise();
	},
	
		/**
	 * Function to calculate number of pages
	 */
	calculatePages : function() {
		var aDeferred = jQuery.Deferred();
		var element = jQuery('#totalPageCount');
		var totalPageNumber = element.text();
		if(totalPageNumber == ""){
			var totalRecordCount = jQuery('#totalCount').val();
			if(totalRecordCount != '') {
				var recordPerPage = jQuery('#noOfEntries').val();
				if(recordPerPage == '0') recordPerPage = 1;
				pageCount = Math.ceil(totalRecordCount/recordPerPage);
				if(pageCount == 0){
					pageCount = 1;
				}
				element.text(pageCount);
				aDeferred.resolve();
				return aDeferred.promise();
			}
			this.getPageCount().then(function(data){
				var pageCount = data['result']['page'];
				if(pageCount == 0){
					pageCount = 1;
				}
				element.text(pageCount);
				aDeferred.resolve();
			});
		} else{
			aDeferred.resolve();
		}
		return aDeferred.promise();
	},
	
	/**
	 * Function to handle search event
	 */

	searchHandler : function(){
		var aDeferred = jQuery.Deferred();
		var completeParams = this.getCompleteParams();
		completeParams['page'] = 1;
		return this.getPageRecords(completeParams).then(
			function(data){
				aDeferred.resolve(data);
			},

			function(textStatus, errorThrown){
				aDeferred.reject(textStatus, errorThrown);
		});
		return aDeferred.promise();
	},

	/**
	 * Function to register event for Search
	 */
	registerEventForSearch : function(){
		var thisInstance = this;
		jQuery('#popupSearchButton').on('click',function(e){
			jQuery('#totalPageCount').text("");
			thisInstance.searchHandler().then(function(data){
				jQuery('#pageNumber').val(1);
				jQuery('#pageToJump').val(1);
				thisInstance.updatePagination();
			});
		});
	},

	/**
	 * Function to handle Sort
	 */
	sortHandler : function(headerElement){
		var aDeferred = jQuery.Deferred();
		var fieldName = headerElement.data('columnname');
		var sortOrderVal = headerElement.data('nextsortorderval');
		var sortingParams = {
			"orderby" : fieldName,
			"sortorder" : sortOrderVal
		}
		var completeParams = this.getCompleteParams();
		jQuery.extend(completeParams,sortingParams);
		return this.getPageRecords(completeParams).then(
			function(data){
				aDeferred.resolve(data);
			},

			function(textStatus, errorThrown){
				aDeferred.reject(textStatus, errorThrown);
			}
		);
		return aDeferred.promise();
	},

	/**
	 * Function to register Event for Sorting
	 */
	registerEventForSort : function(){
		var thisInstance = this;
		var popupPageContentsContainer = this.getPopupPageContainer();
		popupPageContentsContainer.on('click','.listViewHeaderValues',function(e){
			var element = jQuery(e.currentTarget);
			thisInstance.sortHandler(element).then(function(data){
				thisInstance.updatePagination();
			});
		});
	},

	/**
	 * Function to handle next page navigation
	 */

	nextPageHandler : function(){
		var aDeferred = jQuery.Deferred();
		var pageLimit = jQuery('#pageLimit').val();
		var noOfEntries = jQuery('#noOfEntries').val();
		if(noOfEntries == pageLimit){
			var pageNumber = jQuery('#pageNumber').val();
			var nextPageNumber = parseInt(pageNumber) + 1;
			var pagingParams = {
					"page": nextPageNumber
				}
			var completeParams = this.getCompleteParams();
			jQuery.extend(completeParams,pagingParams);
			this.getPageRecords(completeParams).then(
				function(data){
					jQuery('#pageNumber').val(nextPageNumber);
					jQuery('#pageToJump').val(nextPageNumber);
					aDeferred.resolve(data);
				},

				function(textStatus, errorThrown){
					aDeferred.reject(textStatus, errorThrown);
				}
			);
		}
		return aDeferred.promise();
	},

	/**
	 * Function to handle Previous page navigation
	 */
	previousPageHandler : function(){
		var aDeferred = jQuery.Deferred();
		var pageNumber = jQuery('#pageNumber').val();
		var previousPageNumber = parseInt(pageNumber) - 1;
		if(pageNumber > 1){
			var pagingParams = {
				"page": previousPageNumber
			}
			var completeParams = this.getCompleteParams();
			jQuery.extend(completeParams,pagingParams);
			this.getPageRecords(completeParams).then(
				function(data){
					jQuery('#pageNumber').val(previousPageNumber);
					jQuery('#pageToJump').val(previousPageNumber);
					aDeferred.resolve(data);
				},

				function(textStatus, errorThrown){
					aDeferred.reject(textStatus, errorThrown);
				}
			);
		}
		return aDeferred.promise();
	},

	/**
	 * Function to register event for Paging
	 */
	registerEventForPagination : function(){
		var thisInstance = this;
		jQuery('#listViewNextPageButton').on('click',function(){
			thisInstance.nextPageHandler().then(function(data){
				thisInstance.updatePagination();
			});
		});
		jQuery('#listViewPreviousPageButton').on('click',function(){
			thisInstance.previousPageHandler().then(function(data){
				thisInstance.updatePagination();
			});
		});
		jQuery('#listViewPageJump').on('click',function(e){
			jQuery('#pageToJump').validationEngine('hideAll');
			var element = jQuery('#totalPageCount');
			var totalPageNumber = element.text();
			if(totalPageNumber == ""){
				var totalRecordCount = jQuery('#totalCount').val();
				if(totalRecordCount != '') {
					var recordPerPage = jQuery('#pageLimit').val();
					if(recordPerPage == '0') recordPerPage = 1;
					pageCount = Math.ceil(totalRecordCount/recordPerPage);
					if(pageCount == 0){
						pageCount = 1;
					}
					element.text(pageCount);
					return;
				}
				element.progressIndicator({});
				thisInstance.getPageCount().then(function(data){
					var pageCount = data['result']['page'];
					element.text(pageCount);
					element.progressIndicator({'mode': 'hide'});
			});
		}
		})

		jQuery('#listViewPageJumpDropDown').on('click','li',function(e){
			e.stopImmediatePropagation();
		}).on('keypress','#pageToJump',function(e){
			if(e.which == 13){
				e.stopImmediatePropagation();
				var element = jQuery(e.currentTarget);
				var response = Vtiger_WholeNumberGreaterThanZero_Validator_Js.invokeValidation(element);
				if(typeof response != "undefined"){
					element.validationEngine('showPrompt',response,'',"topLeft",true);
				} else {
					element.validationEngine('hideAll');
					var currentPageElement = jQuery('#pageNumber');
					var currentPageNumber = currentPageElement.val();
					var newPageNumber = parseInt(element.val());
					var totalPages = parseInt(jQuery('#totalPageCount').text());
					if(newPageNumber > totalPages){
						var error = app.vtranslate('JS_PAGE_NOT_EXIST');
						element.validationEngine('showPrompt',error,'',"topLeft",true);
						return;
					}
					if(newPageNumber == currentPageNumber){
						var message = app.vtranslate('JS_YOU_ARE_IN_PAGE_NUMBER')+" "+newPageNumber;
						var params = {
							text: message,
							type: 'info'
						};
						Vtiger_Helper_Js.showMessage(params);
						return;
					}
					var pagingParams = {
						"page": newPageNumber
					}
					var completeParams = thisInstance.getCompleteParams();
					jQuery.extend(completeParams,pagingParams);
					thisInstance.getPageRecords(completeParams).then(
						function(data){
							currentPageElement.val(newPageNumber);
							thisInstance.updatePagination();
							element.closest('.btn-group ').removeClass('open');
						},
						function(textStatus, errorThrown){
						}
					);
				}
				return false;
		}
		});
	},

	registerEventForListViewEntries : function(){
		var thisInstance = this;
		var popupPageContentsContainer = this.getPopupPageContainer();
		popupPageContentsContainer.on('click','.listViewEntries',function(e){
		    thisInstance.getListViewEntries(e);
		});
	},

	/**
	 * Function to get page count and total number of records in list
	 */
	getPageCount : function(){
		var aDeferred = jQuery.Deferred();
		var pageJumpParams = {
			'mode' : "getPageCount"
		}
		var completeParams = this.getCompleteParams();
		jQuery.extend(completeParams,pageJumpParams);
		AppConnector.request(completeParams).then(
			function(data) {
				var response;
				if(typeof data != "object"){
					response = JSON.parse(data);
				} else{
					response = data;
				}
				aDeferred.resolve(response);
			},
			function(error,err){

			}
		);
		return aDeferred.promise();
	},
	
	/**
	 * Function to show total records count in listview on hover
	 * of pageNumber text
	 */
	registerEventForTotalRecordsCount : function(){
		var thisInstance = this;
		jQuery('.pageNumbers').on('hover',function(e){
			var element = jQuery(e.currentTarget);
			var totalRecordsElement = jQuery('#totalCount');
			var totalNumberOfRecords = totalRecordsElement.val();
			if(totalNumberOfRecords == '') {
				thisInstance.getPageCount().then(function(data){
					totalNumberOfRecords = data['result']['numberOfRecords'];
					totalRecordsElement.val(totalNumberOfRecords);
				});
			}
			if(totalNumberOfRecords != ''){
				var titleWithRecords = app.vtranslate("JS_TOTAL_RECORDS")+" "+totalNumberOfRecords;
				element.data('tooltip').options.title = titleWithRecords;
			} else {
				element.data('tooltip').options.title = "";
			}
		})
	},
	
	/**
	 * Function to update Pagining status
	 */
	updatePagination : function(){
		var previousPageExist = jQuery('#previousPageExist').val();
		var nextPageExist = jQuery('#nextPageExist').val();
		var previousPageButton = jQuery('#listViewPreviousPageButton');
		var nextPageButton = jQuery('#listViewNextPageButton');
		var listViewEntriesCount = jQuery('#noOfEntries').val();
		var pageStartRange = jQuery('#pageStartRange').val();
		var pageEndRange = jQuery('#pageEndRange').val();
		var pageJumpButton = jQuery('#listViewPageJump');
		var pages = jQuery('#totalPageCount').text();

		if(pages == 1){
			pageJumpButton.attr('disabled',"disabled");
		}
		if(pages > 1){
			pageJumpButton.removeAttr('disabled');
		}

		if(previousPageExist != ""){
			previousPageButton.removeAttr('disabled');
		} else if(previousPageExist == "") {
			previousPageButton.attr("disabled","disabled");
		}

		if((nextPageExist != "") && (pages >1)){
			nextPageButton.removeAttr('disabled');
		} else if((nextPageExist == "") || (pages == 1)) {
			nextPageButton.attr("disabled","disabled");
		}
		if(listViewEntriesCount != 0){
			var pageNumberText = pageStartRange+" "+app.vtranslate('to')+" "+pageEndRange;
			jQuery('.pageNumbers').html(pageNumberText);
		} else {
			jQuery('.pageNumbers').html("");
		}

	},

	triggerImportPopup : function(e){
		
		var advanceFilter = this.advanceFilterInstance;
		
    	jQuery("#import_leads").on("click", function(){			
	        
	        var callBackFunction = function(data){
	        	
	        	var leadsListForm = jQuery('#dialerfieldsview');
				
	        	leadsListForm.validationEngine(app.validationEngineOptions);
	        	leadsListForm.find('.chz-select').chosen();
	        	
				
				leadsListForm.on('submit',function(e){
					
					var selected_list = jQuery("#all_list").val();					
					if(selected_list == 'new_list') {
						$("#listname").val($("#newlistname").val());
					} else {
						$("#listname").val(jQuery("#all_list").val());
					}
					
					
					//progressIndicatorInstance = jQuery.progressIndicator({'position' : 'html'});
					leadsListForm.find(".spinner-img").show();
                    e.preventDefault();
					
                    var validationResult = jQuery(e.currentTarget).validationEngine('validate');
					
					if(!validationResult){
						 //progressIndicatorInstance.hide();
						leadsListForm.find(".spinner-img").hide();
	                    
                         return false;
					} else {
						
						var formData = leadsListForm.serializeFormData();
						
						AppConnector.request(formData).then(
							function(data){
								
                                //progressIndicatorInstance.hide();
								leadsListForm.find(".spinner-img").hide();
			                    
                                if(data['result'] && data['result'].message){
        							app.hideModalWindow();
        							var  params = {
        	    						title : app.vtranslate('JS_MESSAGE'),
        	    		                text: app.vtranslate('JS_ITEM_ADDED_SUCCESSFULLY'),
        	    		                animation: 'show',
        	    						type: 'success'
        	    					}
        	    					Vtiger_Helper_Js.showPnotify(params);
	        	    			} else {
	        						app.hideModalWindow();
	        						var params = {
	        							title : app.vtranslate('JS_ERROR'),
	        							text : data.error.message
	        						}
	        						Vtiger_Helper_Js.showPnotify(params);
	        					}
								
							}
						);
					}
					
				});
				
	    	}
	    	
	    	var actionParams = {
				module: 'Leads',
				view : 'ImportLeads',
	    	}
			actionParams['advfilterlist'] = JSON.stringify(advanceFilter.getValues());
			
			AppConnector.request(actionParams).then(
     			function(data) {
 					app.showModalWindow(data,function(data){
 						if(typeof callBackFunction == 'function'){
 							callBackFunction(data);
 						}
 					});
     			},
     			function(error,err){}
	     	);
			
		});
		
	},
    
	registerEvents: function(){
		
		var pageNumber = jQuery('#pageNumber').val();
		
		if(pageNumber == 1){
			jQuery('#listViewPreviousPageButton').attr("disabled", "disabled");
		}
		
		this.registerEventForSearch();
		
		this.registerEventForListViewEntries();
		
		var popupPageContainer = jQuery('#popupPageContainer');
		
		if(popupPageContainer.length > 0){
			this.registerEventForTotalRecordsCount();
			this.registerEventForPagination();
			jQuery('.pageNumbers').tooltip();
		}
		
		this.advanceFilterInstance = Vtiger_AdvanceFilter_Js.getInstance(jQuery('.filterContainer'));	
		
		this.triggerImportPopup();
		
	}
});
