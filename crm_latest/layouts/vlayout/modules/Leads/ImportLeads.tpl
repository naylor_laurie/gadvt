{strip}

	<script>
		$(document).ready(function(){
			$("#all_list").chosen();
		});

		function listnameChange(e){
			var selected_list = jQuery(e).val();
			$("#newlistname").val("");
			if(selected_list == 'new_list'){
				$(".newlist").show();
				$("#newlistname").attr('data-validation-engine','validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]');
				$("#listname").val("");
			} else {
				$(".newlist").hide();
				$("#newlistname").removeAttr('data-validation-engine');
			}
		}
	</script>

	<div class = "modalwindow">
	
		<div class = "modal-header contentsBackground">
			<button type = "button" class = "close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Import Leads into GAD</h3>
		</div>
		
		<form class="form-horizontal dialerfieldsview" id="dialerfieldsview" method="post" action="index.php">
			<div class = "modal-body">
				
				<input type="hidden" name="module" value="{$MODULE}" >
				<input type="hidden" name="action" value="ImportLeads" >
				<input type="hidden" name="advanced_filter" id="advanced_filter" value="{Vtiger_Util_Helper::toSafeHTML(ZEND_JSON::encode($ADV_FILTER_FIELDS))}" >

				<input type="hidden" id="listname" name="listname" value="" />
						
				<div class="row-fluid">
					
					<div class = "control-group">
						<label class ="control-label" style = "width:104px;">
							<span class="redColor">*</span>
							<strong> Select List</strong>
						</label>
						<div class = "controls" style = "margin-left:107px;">
							<select id="all_list" class="select chzn-select" name="all_list" onchange = "javascript:listnameChange(this);" data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]">
								<option value="" selected> Select an Option</option>
								{foreach key=LISTVAL item=LISTNAME from=$LIST_NAMES}
									<option value="{$LISTVAL}">{vtranslate($LISTNAME, $MODULE)}</option>
								{/foreach}
							</select> 
						</div>
					</div>
					
					<div class = "control-group newlist" style="display:none;">
						<label class ="control-label" style = "width:104px;">
							<span class="redColor">*</span>
							<strong>Enter List Name</strong>
						</label>
						<div class = "controls" style = "margin-left:107px;">
							<input type="text" id="newlistname" name="newlistname" value=""  data-validation-engine=""/>
						</div>
					</div>
					
					<div class = "control-group ">
						<label class ="control-label" style = "width:104px;">
							<strong>Select Campaign</strong>
						</label>
						<div class = "controls" style = "margin-left:107px;">
							<select id="campaign" class="select chzn-select" name="campaign">
								<option value="" selected> Select an Option</option>
								{foreach key=CAMPAIGN_ID item=CAMPAIGN_NAME from=$CAMPAIGN_NAMES}
									<option value="{$CAMPAIGN_ID}">{vtranslate($CAMPAIGN_NAME, $MODULE)}</option>
								{/foreach}
							</select> 
						</div>
					</div>
					
					<div class = "control-group spinner-img" style="display:none;">
						<img src="test/logo/loading.gif" />

					</div>
					
					
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success nextStep"><strong>Import Now</strong></button>&nbsp;&nbsp;
			</div>
		</form>
	</div>
{/strip}