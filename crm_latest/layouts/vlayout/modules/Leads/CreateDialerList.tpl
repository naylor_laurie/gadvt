{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is:  vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
<div id="popupPageContainer" class="popupBackgroundColor" style = "margin-top:65px;">
	<div class="paddingLeftRight10px">
		<input type="hidden" id="parentModule" value="{$SOURCE_MODULE}"/>
	    <input type="hidden" id="module" value="{$MODULE}"/>
	    <input type="hidden" id="parent" value="{$PARENT_MODULE}"/>
	    <input type="hidden" id="sourceRecord" value="{$SOURCE_RECORD}"/>
	    <input type="hidden" id="sourceField" value="{$SOURCE_FIELD}"/>
	    <input type="hidden" id="url" value="{$GETURL}" />
	    <input type="hidden" id="multi_select" value="{$MULTI_SELECT}" />
	    <input type="hidden" id="currencyId" value="{$CURRENCY_ID}" />
	    <input type="hidden" id="relatedParentModule" value="{$RELATED_PARENT_MODULE}"/>
	    <input type="hidden" id="relatedParentId" value="{$RELATED_PARENT_ID}"/>
	    <input type="hidden" id="view" value="{$VIEW}"/>
    
	    <form class="form-horizontal popupSearchContainer">
   			<div class="control-group margin0px">
        
		        {assign var=ALL_CONDITION_CRITERIA value=$ADVANCE_CRITERIA[1] }
				{assign var=ANY_CONDITION_CRITERIA value=$ADVANCE_CRITERIA[2] }
		
				{if empty($ALL_CONDITION_CRITERIA) }
					{assign var=ALL_CONDITION_CRITERIA value=array()}
				{/if}
			
				{if empty($ANY_CONDITION_CRITERIA) }
					{assign var=ANY_CONDITION_CRITERIA value=array()}
				{/if}
			
				<div class="filterContainer">
					<input type="hidden" name="date_filters" data-value='{Vtiger_Util_Helper::toSafeHTML(ZEND_JSON::encode($DATE_FILTERS))}' />
					<input type=hidden name="advanceFilterOpsByFieldType" data-value='{ZEND_JSON::encode($ADVANCED_FILTER_OPTIONS_BY_TYPE)}' />
					
					{foreach key=ADVANCE_FILTER_OPTION_KEY item=ADVANCE_FILTER_OPTION from=$ADVANCED_FILTER_OPTIONS}
						{$ADVANCED_FILTER_OPTIONS[$ADVANCE_FILTER_OPTION_KEY] = vtranslate($ADVANCE_FILTER_OPTION, $MODULE)}
					{/foreach}
					
					<input type=hidden name="advanceFilterOptions" data-value='{ZEND_JSON::encode($ADVANCED_FILTER_OPTIONS)}' />
					<div class="allConditionContainer conditionGroup contentsBackground well">
						<div class="header">
							<span><strong>{vtranslate('LBL_ALL_CONDITIONS',$MODULE)}</strong></span>
							&nbsp;
							<span>({vtranslate('LBL_ALL_CONDITIONS_DESC',$MODULE)})</span>
						</div>
						<div class="contents">
							<div class="conditionList">
								{foreach item=CONDITION_INFO from=$ALL_CONDITION_CRITERIA['columns']}
									{include file='AdvanceFilterCondition.tpl'|@vtemplate_path:$QUALIFIED_MODULE RECORD_STRUCTURE=$RECORD_STRUCTURE CONDITION_INFO=$CONDITION_INFO MODULE=$MODULE}
								{/foreach}
								{if count($ALL_CONDITION_CRITERIA) eq 0}
									{include file='AdvanceFilterCondition.tpl'|@vtemplate_path:$QUALIFIED_MODULE RECORD_STRUCTURE=$RECORD_STRUCTURE MODULE=$MODULE CONDITION_INFO=array()}
								{/if}
							</div>
							<div class="hide basic">
								{include file='AdvanceFilterCondition.tpl'|@vtemplate_path:$QUALIFIED_MODULE RECORD_STRUCTURE=$RECORD_STRUCTURE CONDITION_INFO=array() MODULE=$MODULE NOCHOSEN=true}
							</div>
							<div class="addCondition">
								<button type="button" class="btn"><strong>{vtranslate('LBL_ADD_CONDITION',$MODULE)}</strong></button>
							</div>
							<div class="groupCondition">
								{assign var=GROUP_CONDITION value=$ALL_CONDITION_CRITERIA['condition']}
								{if empty($GROUP_CONDITION)}
									{assign var=GROUP_CONDITION value="and"}
								{/if}
								<input type="hidden" name="condition" value="" />
							</div>
						</div>
					</div>
					<div class="anyConditionContainer conditionGroup contentsBackground well">
						<div class="header">
							<span><strong>{vtranslate('LBL_ANY_CONDITIONS',$MODULE)}</strong></span>
							&nbsp;
							<span>({vtranslate('LBL_ANY_CONDITIONS_DESC',$MODULE)})</span>
						</div>
						<div class="contents">
							<div class="conditionList">
							{foreach item=CONDITION_INFO from=$ANY_CONDITION_CRITERIA['columns']}
								{include file='AdvanceFilterCondition.tpl'|@vtemplate_path:$QUALIFIED_MODULE RECORD_STRUCTURE=$RECORD_STRUCTURE CONDITION_INFO=$CONDITION_INFO MODULE=$MODULE CONDITION="or"}
							{/foreach}
							{if count($ANY_CONDITION_CRITERIA) eq 0}
								{include file='AdvanceFilterCondition.tpl'|@vtemplate_path:$QUALIFIED_MODULE RECORD_STRUCTURE=$RECORD_STRUCTURE MODULE=$MODULE CONDITION_INFO=array() CONDITION="or"}
							{/if}
							</div>
							<div class="hide basic">
								{include file='AdvanceFilterCondition.tpl'|@vtemplate_path:$QUALIFIED_MODULE RECORD_STRUCTURE=$RECORD_STRUCTURE MODULE=$MODULE CONDITION_INFO=array() CONDITION="or" NOCHOSEN=true}
							</div>
							<div class="addCondition">
								<button type="button" class="btn"><strong>{vtranslate('LBL_ADD_CONDITION',$MODULE)}</strong></button>
							</div>
						</div>
					</div>
				</div>
				<span style="margin-left:0px;" class="actions span6">
			    	<input type="button" value="Search Leads" id="popupSearchButton" />&nbsp;&nbsp;and&nbsp;&nbsp;
			    	<input type="button" value="Save as List" id="savelist" />&nbsp;&nbsp;
			    	<!--<input type="button" value="Import Now" id="importnow" />-->
			    </span>
			    </span>
    			
	        </div>
    	</form>
   	</div>
    
    <div class="popupPaging">
	    <div class="row-fluid">
	    
		    <span class="actions span6">
		    	&nbsp;
		    </span>
            
            <span class="span6">
                <span class="pull-right">
                    <span class="pageNumbers alignTop" data-placement="bottom" data-original-title="">
                    {if !empty($LISTVIEW_ENTRIES)}{$PAGING_MODEL->getRecordStartRange()} {vtranslate('LBL_to', $MODULE)} {$PAGING_MODEL->getRecordEndRange()}{/if}
                </span>
                <span class="pull-right btn-group">
                    <button class="btn" id="listViewPreviousPageButton" {if !$PAGING_MODEL->isPrevPageExists()} disabled {/if}><span class="icon-chevron-left"></span></button>
                    <button class="btn dropdown-toggle" type="button" id="listViewPageJump" data-toggle="dropdown" {if $PAGE_COUNT eq 1} disabled {/if}>
                        <i class="vtGlyph vticon-pageJump" title="{vtranslate('LBL_LISTVIEW_PAGE_JUMP',$moduleName)}"></i>
                    </button>
                    <ul class="listViewBasicAction dropdown-menu" id="listViewPageJumpDropDown">
                        <li>
                            <span class="row-fluid">
                                <span class="span3 pushUpandDown2per"><span class="pull-right">{vtranslate('LBL_PAGE',$moduleName)}</span></span>
                                <span class="span4">
                                    <input type="text" id="pageToJump" class="listViewPagingInput" value="{$PAGE_NUMBER}"/>
                                </span>
                                <span class="span2 textAlignCenter pushUpandDown2per">
                                    {vtranslate('LBL_OF',$moduleName)}&nbsp;
                                </span>
                                <span class="span2 pushUpandDown2per" id="totalPageCount">{$PAGE_COUNT}</span>
                            </span>
                        </li>
                    </ul>
                    <button class="btn" id="listViewNextPageButton" {if (!$PAGING_MODEL->isNextPageExists()) or ($PAGE_COUNT eq 1)} disabled {/if}><span class="icon-chevron-right"></span></button>
                </span>
            </span>
 	   </div>
	</div>
	
	<div class = "clearfix">&nbsp;</div>
	
	<div id="popupContents" class="paddingLeftRight10px">
		{include file='CreateDialerListContents.tpl'|vtemplate_path:$MODULE_NAME}
	</div>
</div>
{/strip}