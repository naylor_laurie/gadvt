{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is:  vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
	<div class="modalwindow">
		<div class="modal-header contentsBackground">
            <button type="button" class="btn btn-success" style = "float:right;" id = "saveLog">Save</button>
            <h3>Choose Call Status</h3>
        </div>
        
        <form class="form-horizontal" id = "AddActivityLogForm" name = "AddActivityLogForm" method="post">
        	<input type="hidden" name="module" value="PBXManager">
        	<input type="hidden" name="action" value="SaveCallLog">        
        	<input type="hidden" name="record" value="{$RECORD}">
         	<input type="hidden" name="activity_value" value="0">
          	
          	<div class="modal-body">
                <div class="row-fluid">
					
					<div class="control-group">
                        <label class="control-label">
                          <b> Call Status:</b>
                        </label>
                        <div class="controls">
	                   		<select name="lead_status" id="lead_status">
	                   		<option value="">Select An Option</option>
	                   			{html_options values=$LEADSTATUS output=$LEADSTATUS selected = $LEAD_STATUS}
	                   		</select>
                   		</div>
					</div>
					
					<div class="control-group">
                        <label class="control-label">
                       		<b>Comments :</b>
                        </label>
                        <div class="controls">
                   			<textarea id="commentcontent" name="commentcontent" data-validator='{Zend_Json::encode([['name'=>'commentcontent']])}' 
                                 data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]"> </textarea>
                   		</div>
                    </div>
                   	<button id="AddActivity" class="btn btn-success">Add Activity</button>
				 </div>
            </div>
           	
           	<div id = "ActivityForm" class="hide">
         	  	<div class="row-fluid">
                   <div class="control-group">
                        <label class="control-label">
                        <span class="redColor">*</span>
                       		Activity Type
                        </label>
                        <div class="controls">
                   			<select name="activity_type" id="activity_type" data-validator='{Zend_Json::encode([['name'=>'activity_type']])}' 
                                 data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]">
                   			<option value="">Select An Option</option>
                   				{html_options values=$ACTIVITY_TYPE output=$ACTIVITY_TYPE selected=$ACTIVITY_TYPE[0]}
                   			</select>
                   		</div>
                   </div>
                   <div class="control-group">
                        <label class="control-label">
                        <span class="redColor">*</span>
                       		Start Date & Time:
                        </label>
                        <div class="controls">
                   	       <input type="text" name="date"  class = "start_date" data-validator='{Zend_Json::encode([['name'=>'date']])}' 
                                 data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" style="width:80%;"/><span class="add-on"><i class="icon-calendar"></i></span><br/>
                   	       <input type="text" id="time" data-validator='{Zend_Json::encode([['name'=>'time']])}' 
                                 data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]" name="time" style="width:45%;"/><span class="add-on cursorPointer"><i class="icon-time"></i></span> 
                   		</div>
                   </div>
                   <div class="control-group">
                        <label class="control-label">
                        <span class="redColor">*</span>
                      		 Subject
                        </label>
                        <div class="controls">
                   			<input type="text" id="subject" name="subject" data-validator='{Zend_Json::encode([['name'=>'subject']])}' 
                                 data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]"/> 
                   		</div>
                   </div>
                   <div class="control-group">
                        <label class="control-label">
                        <span class="redColor">*</span>
                       		Assigned To
                        </label>
                        <div class="controls">
                           <select name="assigned_to" id="assigned_to" data-validator='{Zend_Json::encode([['name'=>'assigned_to']])}' 
                                 data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]">
                   			<option value="">Select an Option</option>
                   				{html_options values=$USERID output=$USERNAME}
                   			</select>
                   		</div>
                  </div>
                  <div class="control-group">
                        <label class="control-label">
                      		 Priority
                        </label>
                        <div class="controls">
                       		<select name="priority" id="priority">
	                   			 <option value="select an option">Select An Option</option>
	                   				{html_options values=$PRIORITY output=$PRIORITY}
                   			</select>
                   		</div>
                  </div>
                </div>
            </div>
       </form>
    </div>
{/strip}
