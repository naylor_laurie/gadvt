{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *
 ********************************************************************************/
-->*}
        <script type="text/javascript" src="layouts/vlayout/modules/Vtiger/resources/Export.js?v=6.2.0"></script>
{strip}
<div id="toggleButton" class="toggleButton" title="{vtranslate('LBL_LEFT_PANEL_SHOW_HIDE', 'Vtiger')}">
				<i id="tButtonImage" class="{if $LEFTPANELHIDE neq '1'}icon-chevron-left{else}icon-chevron-right{/if}"></i>
			</div>&nbsp
<div style="padding-left: 15px;">
	<form action="index.php" enctype="multipart/form-data" method="POST" name="importAdvanced">
            <input type="hidden" name="module" value="Leads" />
            <input type="hidden" name="view" value="ExportGadStep3" />
            <input type="hidden" name="list_id" value="{$LIST_ID}" />
            <input type="hidden" name="list_name" value="{$LIST_NAME}" />
           <input type="hidden" name="list_column" value="{$LIST_COLUMN}" />



		<input type="hidden" name="mode" value="Export" />

		<input type="hidden" id="mandatory_fields" name="mandatory_fields" value='{$ENCODED_MANDATORY_FIELDS}' />

		<table style=" width:90%;margin-left: 5%" cellpadding="2" cellspacing="12" class="searchUIBasic">
			<tr>
				<td class="font-x-large" align="left" colspan="2">
					<strong>Export to GAD </strong>
				</td>
			</tr>
			{if $ERROR_MESSAGE neq ''}
			<tr>
				<td class="style1" align="left" colspan="2">
					{$ERROR_MESSAGE}
				</td>
			</tr>
			{/if}
			<tr>
				<td class="leftFormBorder1" colspan="2" valign="top">
				{include file='Export_map.tpl'|@vtemplate_path:'Vtiger'}
				</td>
			</tr>
			<tr>
				<td align="left" colspan="2">
				{include file='Export_Advanced_Buttons.tpl'|@vtemplate_path:'Vtiger'}
				</td>
			</tr>
		</table>
	</form>
{/strip}

