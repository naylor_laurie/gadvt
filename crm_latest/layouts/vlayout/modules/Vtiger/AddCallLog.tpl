{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is:  vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
	<div class="modalwindow">
		<div class="modal-header contentsBackground">
            <button type="button" class="btn btn-success" style = "float:right;" id = "saveLog">Save</button>
            <h3>Add Call Log</h3>
        </div>
        
        <form class="form-horizontal" id = "AddActivityLogForm" name = "AddActivityLogForm" method="post">
        	<input type="hidden" name="module" value="PBXManager">
        	<input type="hidden" name="action" value="SaveCallLog">        
        	<input type="hidden" name="record" value="{$RECORD}">
         	
			<div class="modal-body">
                <div class="row-fluid">
					
					<div class="control-group">
                        <label class="control-label">
                          <b> Lead Status:</b>
                        </label>
                        <div class="controls">
	                   		<select name="lead_status" id="lead_status">
	                   		<option value="">Select An Option</option>
	                   			{html_options values=$LEADSTATUS output=$LEADSTATUS selected = $LEAD_STATUS}
	                   		</select>
                   		</div>
					</div>
					
					<div class="control-group">
                        <label class="control-label">
                       		<b>Comments :</b>
                        </label>
                        <div class="controls">
                   			<textarea id="commentcontent" name="commentcontent" data-validator='{Zend_Json::encode([['name'=>'commentcontent']])}' 
                                 data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]"> </textarea>
                   		</div>
                    </div>
                 </div>
            </div>
       </form>
    </div>
{/strip}