{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
*
 ********************************************************************************/
-->*}
{strip}
<div class="commentDiv">
	<div class="singleComment">
		<div class="commentInfoHeader row-fluid" data-noteid="{$NOTE->getId()}">
			<div class="commentTitle" id="{$NOTE->getId()}">
				<div class="row-fluid">
				     <div class="span1">
						{assign var=IMAGE_PATH value=$NOTE->getImagePath()}
						<img class="alignMiddle pull-left" src="{if !empty($IMAGE_PATH)}{$IMAGE_PATH}{else}{vimage_path('DefaultUserIcon.png')}{/if}">
					</div>
				    <div class="span11 noteInfo">
						{assign var=COMMENTOR value=$NOTE->getNoteByModel()}
						<div class="inner">
							<span class="commentorName pull-left"><strong>{$COMMENTOR->getName()}</strong></span>
							<span class="pull-right">
								<p class="muted"><small title="{Vtiger_Util_Helper::formatDateTimeIntoDayString($NOTE->getNoteTime())}">{Vtiger_Util_Helper::formatDateDiffInStrings($NOTE->getNoteTime())}</small></p>
							</span>
							<div class="clearfix"></div>
						</div>
						<div class="commentInfoContent">
							{nl2br($NOTE->get('vnotetitle'))}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row-fluid commentActionsContainer">
		   <div class="row-fluid editedStatus"  name="editStatus">
					<div class="row-fluid">
						{if $NOTE->getNoteTime() neq $NOTE->getModifiedTime()}
							<span class="pull-right">
									<p class="muted"><small><em>{vtranslate('LBL_MODIFIED',$MODULE_NAME)}</em></small>&nbsp;<small title="{Vtiger_Util_Helper::formatDateTimeIntoDayString($NOTE->getModifiedTime())}" class="commentModifiedTime">{Vtiger_Util_Helper::formatDateDiffInStrings($NOTE->getModifiedTime())}</small></p>
								</span>
						{/if}
					</div>
			</div>
			<div class="row-fluid commentActionsDiv">
			  <div class="pull-right commentActions">
			  	 {if $VNOTE_MODULE_MODEL->isPermitted('EditView')}
						<span>
							{if $CURRENTUSER->getId() eq $NOTE->get('userid')}
							<a class="cursorPointer editNote feedback">
								{vtranslate('LBL_EDIT',$MODULE_NAME)}
							</a>
							{/if}
						</span>
				 {/if}
				</div> 
			</div>
		</div>
	</div>
<div>
{/strip}

