{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *
 ********************************************************************************/
-->*}
{strip}
<div id="toggleButton" class="toggleButton" title="{vtranslate('LBL_LEFT_PANEL_SHOW_HIDE', 'Vtiger')}">
				<i id="tButtonImage" class="{if $LEFTPANELHIDE neq '1'}icon-chevron-left{else}icon-chevron-right{/if}"></i>
			</div>&nbsp
<div style="padding-left: 15px;">

		<table style=" width:90%;margin-left: 5%" cellpadding="2" cellspacing="12" class="searchUIBasic">
			<tr>
				<td class="font-x-large" align="left" colspan="2">
					<strong>Export to GAD</strong>
				</td>
			</tr>
			{if $ERROR_MESSAGE neq ''}
			<tr>
				<td class="style1" align="left" colspan="2">
					{$ERROR_MESSAGE}
				</td>
			</tr>
			{else}
			<tr>
                                <td class="font-x-large" align="left" colspan="2">
                                        <strong>Export Completed</strong><br />
					Success = {$success} <br />
					Failed = {$failed}
                                </td>
			</tr>
			{/if}
			<tr><td colspan="2">
<button name="goback" onclick="window.close()" class="edit btn btn-danger"><strong>Close</strong></button>
 </td></tr>
		</table>
	</form>
{/strip}
