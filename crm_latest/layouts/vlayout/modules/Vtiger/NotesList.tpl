{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
*
 ********************************************************************************/
-->*}
{strip}
{if !empty($PARENT_NOTES)}
	<ul class="liStyleNone">
	  {if is_array($PARENT_NOTES)}
			{foreach key=Index item=NOTE from=$PARENT_NOTES}
				{assign var=PARENT_NOTE_ID value=$NOTE->getId()}
				<li class="commentDetails">
				{include file='Note.tpl'|@vtemplate_path NOTE=$NOTE VNOTE_MODULE_MODEL=$VNOTE_MODULE_MODEL}
				</li><br>	
			{/foreach}
		{else}
			{include file='Note.tpl'|@vtemplate_path COMMENT=$PARENT_NOTES}
		{/if}
	</ul>
{else}
	{include file="NoNotes.tpl"|@vtemplate_path}
{/if}
{/strip}