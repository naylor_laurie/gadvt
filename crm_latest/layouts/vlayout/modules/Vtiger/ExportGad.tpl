{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
* ("License"); You may not use this file except in compliance with the License
* The Original Code is:  vtiger CRM Open Source
* The Initial Developer of the Original Code is vtiger.
* Portions created by vtiger are Copyright (C) vtiger.
* All Rights Reserved.
*
********************************************************************************/
-->*}
{strip}
<div id="toggleButton" class="toggleButton" title="{vtranslate('LBL_LEFT_PANEL_SHOW_HIDE', 'Vtiger')}">
	<i id="tButtonImage" class="{if $LEFTPANELHIDE neq '1'}icon-chevron-right {else} icon-chevron-left{/if}"></i>
</div>&nbsp
    <div style="padding-left: 15px;">
            
            <div class="row-fluid">
                <div class="span">&nbsp;</div>
                <div class="span8">
                    <h4>{vtranslate('LBL_EXPORT_RECORDS',$MODULE)}</h4>
                    <div class="well exportContents marginLeftZero">
                        <div class="row-fluid">
        <form id="exportForm" class="form-horizontal row-fluid" method="post" action="index.php">
            <input type="hidden" name="module" value="Leads" />
            <input type="hidden" name="view" value="ExportGadStep2" />
            <input type="hidden" name="type" value="uselist" />
            <input type="hidden" name="list_name" value="{$LIST_NAME}" />
            <input type="hidden" name="list_column" value="{$LIST_COLUMN}" />


                            <div class="row-fluid" style="height:30px">
                                <div class="span6 textAlignRight row-fluid">
                                    <div class="span8">Choose a GAD List &nbsp;</div>
                                    <div class="span3">
						<div class = "controls" style = "margin-left:107px;">
							<select id="all_list" class="select chzn-select" name="all_list" onchange = "javascript:listnameChange(this);" data-validation-engine="validate[required, funcCall[Vtiger_Base_Validator_Js.invokeValidation]]">
								<option value="" selected> Select an Option</option>
								{foreach key=LISTVAL item=LISTNAME from=$LIST_NAMES}
									<option value="{$LISTVAL}">{vtranslate($LISTNAME, $MODULE)}</option>
								{/foreach}
							</select> 
						</div>                                        
                                        
                                    </div>
                                </div>
                        </div>
                                                                            <br>

                    <div class="textAlignCenter">
                        <button class="btn btn-success" type="submit"><strong>{vtranslate($MODULE, $MODULE)}&nbsp;{vtranslate($SOURCE_MODULE, $MODULE)}</strong></button>
                        <a class="cancelLink" type="reset" onclick='window.history.back()'>{vtranslate('LBL_CANCEL', $MODULE)}</a>
                    </div> 
        </form>                                                        
        <form id="exportForm" class="form-horizontal row-fluid" method="post" action="index.php">
            <input type="hidden" name="module" value="Leads" />
            <input type="hidden" name="view" value="ExportGadStep2" />
            <input type="hidden" name="type" value="createnew" />
            <input type="hidden" name="list_name" value="{$LIST_NAME}" />
            <input type="hidden" name="list_column" value="{$LIST_COLUMN}" />


                    <hr /> <div style='text-align:center;font-weight: bold'> OR </div> <hr />
                        <div class="row-fluid" style="height:30px">
                            <div class="span6 textAlignRight row-fluid">
                                <div class="span8">List Id&nbsp;</div>
                                <div class="span3"><input type="text" name="list_id" value="" /></div>
                            </div>
                        </div>
                        <div class="row-fluid" style="height:30px">
                            <div class="span6 textAlignRight row-fluid">
                                <div class="span8">List Name&nbsp;</div>
                                <div class="span3"><input type="text" name="new_list_name" value="" /></div>
                            </div>
                        </div>
                        <div class="row-fluid" style="height:30px">
                                                        <div class="span6 textAlignRight row-fluid">

                                 <div class="span8">Choose Campaign&nbsp;</div>
                                 <div class="span3">
							<select id="campaign" class="select chzn-select" name="campaign">
								<option value="" selected> Select an Option</option>
								{foreach key=CAMPAIGN_ID item=CAMPAIGN_NAME from=$CAMPAIGN_NAMES}
									<option value="{$CAMPAIGN_ID}">{vtranslate($CAMPAIGN_NAME, $MODULE)}</option>
								{/foreach}
							</select> 
                                 </div>                            </div> </div>
                        </div>
                    </div>
                    <br>
                    <div class="textAlignCenter">
                        <button class="btn btn-success" type="submit"><strong>Create List and {vtranslate($MODULE, $MODULE)}&nbsp;{vtranslate($SOURCE_MODULE, $MODULE)} </strong></button>
                        <a class="cancelLink" type="reset" onclick='window.history.back()'>{vtranslate('LBL_CANCEL', $MODULE)}</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
{/strip}
