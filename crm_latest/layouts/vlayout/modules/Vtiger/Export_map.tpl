{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *
 ********************************************************************************/
-->*}

<table width="" cellspacing="0" cellpadding="10" class="importContents">
	<tr>
		<td>
			<strong>Step 2</strong>
		</td>
		<td>
			<span class="big">Map Lead Column To Gad Fields</span>
		</td>
		<td>
	        </td>
	</tr>
        <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
<!--            <div id="savedMapsContainer" class="textAlignRight pull-right">
                                {include file="Export_Saved_Maps.tpl"|@vtemplate_path:'Vtiger'}
-->
                        </div>
                </td>
        </tr>

	<tr>
		<td>&nbsp;</td>
        <td colspan="2">
			<table width="" cellspacing="0" cellpadding="2" class="listRow table table-bordered table-condensed listViewEntriesTable">
				<thead>
					<tr class="listViewHeaders">
						<th width="25%"><a>Gad Label</a></th>
						<th width="23%"><a>CRM Field</a></th>
					</tr>
				</thead>
				<tbody>
					{foreach key=_HEADER_NAME item=_FIELD_VALUE from=$LIST_FIELDS name="headerIterator"}
					{assign var="_COUNTER" value=$smarty.foreach.headerIterator.iteration}
					<tr class="fieldIdentifier" id="fieldIdentifier{$_COUNTER}">
						{if $HAS_HEADER eq true}
						<td class="cellLabel">
							<span name="header_name">{$_HEADER_NAME}
</span>
						</td>
						{/if}
						<td class="cellLabel" width="200px">
							<span>{$_FIELD_VALUE|@textlength_check}{if $_FIELD_VALUE|@textlength_check eq "phone_number"} * {/if}</span>
						</td>
						<td class="cellLabel">
							<input type="hidden" name="row_counter" value="{$_COUNTER}" />
							<select name="mapped_fields[{$_COUNTER}]" class="txtBox chzn-select {if $_FIELD_VALUE|@textlength_check eq "phone_number"} required {/if}"  >
								<option value="">{'LBL_NONE'|@vtranslate:$FOR_MODULE}</option>
								{foreach key=_FIELD_NAME item=_FIELD_INFO from=$AVAILABLE_FIELDS}
								{assign var="_TRANSLATED_FIELD_LABEL" value=$_FIELD_INFO->getFieldLabelKey()|@vtranslate:$FOR_MODULE}
								<option value="{$_FIELD_NAME}" {if decode_html($_HEADER_NAME) eq $_TRANSLATED_FIELD_LABEL} selected {/if} data-label="{$_TRANSLATED_FIELD_LABEL}">{$_TRANSLATED_FIELD_LABEL}</option>
								{/foreach}
							</select>
						</td>
					</tr>
					{/foreach}
			</tbody>
			</table>
		</td>
	</tr>
<!--
	<tr>
		<td>&nbsp;</td>
        <td align="right" colspan="2">
            <input type="checkbox" name="save_map" id="save_map"/>&nbsp;Save Template &nbsp;&nbsp;
            <input type="text" name="save_map_as" id="save_map_as"/>
		</td>
	</tr>
-->
</table>
{include file="Export_Default_Values_Widget.tpl"|@vtemplate_path:'Vtiger'}
