{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
*
 ********************************************************************************/
-->*}

{* Change to this also refer: Recentnotes.tpl *}
{assign var="note_TEXTAREA_DEFAULT_ROWS" value="2"}

<div class="commentContainer">
	<div class="commentTitle row-fluid">
		{if $VNOTE_MODULE_MODEL->isPermitted('EditView')}
			<div class="addCommentBlock">
				<div>
					<textarea name="notecontent" rows="{$note_TEXTAREA_DEFAULT_ROWS}" class="notecontent"  placeholder="{vtranslate('LBL_ADD_YOUR_note_HERE', $MODULE_NAME)}"></textarea>
				</div>
				<div class="pull-right">
					<button class="btn btn-success savenote" type="button" data-mode="add"><strong>{vtranslate('LBL_POST', $MODULE_NAME)}</strong></button>
				</div>
			</div>
		{/if}
	</div>
	<br>
	<div class="commentsList commentsBody">
		{include file='NotesList.tpl'|@vtemplate_path VNOTE_MODULE_MODEL=$VNOTE_MODULE_MODEL}
	</div>
	
	   <div class="hide basicEditNoteBlock" style="min-height: 120px;">
		<div class="row-fluid">
			<span class="span1">&nbsp;</span>
			<div class="span11">
				<textarea class="notecontenthidden fullWidthAlways" name="notecontent" rows="{$note_TEXTAREA_DEFAULT_ROWS}"></textarea>
			</div>
		</div>
		<div class="pull-right">
			<button class="btn btn-success savenote" type="button" data-mode="edit"><strong>{vtranslate('LBL_POST', $MODULE_NAME)}</strong></button>
			<a class="cursorPointer closeNoteBlock cancelLink" type="reset">{vtranslate('LBL_CANCEL', $MODULE_NAME)}</a>
		</div>
	</div>
</div>